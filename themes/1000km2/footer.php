			<footer class="row">
				<div class="span12 footer-inner">
					<div class="row-fluid">
						<div class="span2">
							<span class="designer">Designed by</span>
							<span class="designer-name">Chrisna Bosker</span>
						</div>
						<div class="span8 copyright">
							&copy; Copyright 2013 1000km Challenge. All Rights Reserved.
						</div>
						<div class="span2">
							<span class="designer">Website by</span>
							<span class="designer-name"><a href="http://www.xilix.co.za">XiliX</a></span>
						</div>
					</div>
				</div>
			</footer>
		</section>
		<?php wp_footer(); ?>
	</body>
</html>