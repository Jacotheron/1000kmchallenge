<?php get_header(); ?>
			<section class="row content-container">
				<div class="section-inner span12">
					<div class="row-fluid">
						<article class="span8">
							<?php if (have_posts()) : ?>
								<?php while (have_posts()) : the_post(); ?>
									<div class="post-page">
										<h1><?php the_title(); ?></h1>
										<?php the_content('<span class="read-more btn btn-success">Read More...</span>'); ?>
									</div>
								<?php endwhile; ?>
							<?php else: ?>
									<div class="post-page">
										<h1>Page Not Found</h1>
										<p>The page you are looking for could not be found.</p>
										<p>Please try searching or make use of our easy to use navigation.</p>
									</div>
							<?php endif; ?>
						</article>
						<?php get_sidebar(); ?>
					</div>
				</div>
			</section>
			<?php get_footer(); ?>