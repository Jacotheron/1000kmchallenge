<!doctype html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<title><?php wp_title(""); ?></title>
		<?php wp_head(); ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>"/>
		<!--[if lt IE 9]>
			<script src="<?php bloginfo('template_directory'); ?>/js/html5.js"></script>
		<![endif]-->
	</head>
	<body>
		<section class="container">
			<header class="row">
				<div class="header-inner span12">
					<div class="logo pull-left">
						<a href="<?php echo home_url(); ?>/">
							<img src="<?php bloginfo('template_directory'); ?>/img/1000km-logo.png" alt="1000km Challenge" />
						</a>
					</div>
					<div class="search-box pull-right">
						<form action="<?php echo bp_search_form_action(); ?>" method="post" class="form-search">
							<div class="input-append">
								<input type="text" name="search-terms" class="span2 search-query" value="<?php echo isset( $_REQUEST['s'] ) ? esc_attr( $_REQUEST['s'] ) : ''; ?>" /><?php echo bp_search_form_type_select(); ?><?php wp_nonce_field( 'bp_search_form' ); ?>
								<button type="submit" name="search-submit" class="btn btn-success">Search</button>
							</div>
						</form>
					</div>
					<div class="social pull-right">
						<div  class="btn-group">
							<?php wp_nav_menu(
								array(
									'container'       => false,
									'menu' 		  => 'secondary',
									'echo'            => true,
									'items_wrap'      => '%3$s',
									'depth'           => 1,
									'walker' 	  => new Theme_Secondary_Nav_Walker(),
								)
							); ?>
						</div>
					</div>
				</div>
				<div class="header-inner span12">
					<nav class="navbar">
						<div class="navbar-inner">
							<?php wp_nav_menu(
								array(
									'container'       => '',
									'menu' 			  => 'main',
									'container_class' => '',
									'menu_class'      => 'nav',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									'depth'           => 0,
									'walker' 			=> new Bootstrap_Walker(),
								)
							); ?>
						</div>
					</nav>
				</div>
			</header>
			<?php if(is_page()){ ?>
			<div class="main-image row">
				<div class="span12">
					<img src="<?php bloginfo('template_directory'); ?>/img/athletes.jpg" alt="Our Passion. Our Pride." />
					<div class="image-overlay">Our Passion. Our Pride.</div>
				</div>
			</div>
			<?php } ?>