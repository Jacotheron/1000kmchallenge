<?php get_header(); ?>
			<section class="row content-container">
				<div class="section-inner span12">
					<div class="row-fluid">
						<article class="span8">
							<?php theme_pagination(); ?>
							<?php if (have_posts()) : ?>
								<?php while (have_posts()) : the_post(); ?>
									<div class="post-page">
										<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h2>
										<?php the_content('<span class="read-more btn btn-success">Read More...</span>'); ?>
									</div>
								<?php endwhile; ?>
							<?php else: ?>
									<div class="post-page">
										<h1>Page Not Found</h1>
										<p>The page you are looking for could not be found.</p>
										<p>Please try searching or make use of our easy to use navigation.</p>
									</div>
							<?php endif; ?>
							<?php theme_pagination(); ?>
						</article>
						<?php get_sidebar(); ?>
					</div>
				</div>
			</section>
			<?php get_footer(); ?>