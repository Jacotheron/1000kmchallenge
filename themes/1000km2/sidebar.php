						<aside class="span4">
							<ul>
								<?php if ( is_user_logged_in() ) : ?>
									<li id="sidebar-me">
										<h2>Me, Myself &amp; I</h2>
										<a href="<?php echo bp_loggedin_user_domain(); ?>">
											<?php bp_loggedin_user_avatar( 'type=thumb&width=50&height=50' ); ?>
										</a>
										<h4><?php echo bp_core_get_userlink( bp_loggedin_user_id() ); ?></h4>
										<div class="btn-group">
											<a class="btn" href="<?php echo bp_loggedin_user_domain(); ?>"><?php bp_loggedin_user_fullname(); ?></a>
											<button class="btn dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
											<ul class="dropdown-menu">
												<li><a href="<?php echo bp_loggedin_user_domain(); ?>runninglog/"><i class="icon-calendar"></i> Running Log</a></li>
												<li><a href="<?php echo bp_loggedin_user_domain(); ?>runninglog/training/"><i class="icon-th-list"></i> Training Log</a></li>
												<li><a href="<?php echo bp_loggedin_user_domain(); ?>runninglog/awards/"><i class="icon-certificate"></i> Awards</a></li>
												<li class="divider"></li>
												<li><a href="<?php echo bp_loggedin_user_domain(); ?>messages/"><i class="icon-envelope"></i> Messages</a></li>
												<li><a href="<?php echo bp_loggedin_user_domain(); ?>profile/edit/"><i class="icon-user"></i> Edit Profile</a></li>
												<li><a href="<?php echo bp_loggedin_user_domain(); ?>profile/edit/group/3"><i class="icon-tasks"></i> Log Settings</a></li>
												<li><a href="<?php echo bp_loggedin_user_domain(); ?>settings/"><i class="icon-wrench"></i> Preferences</a></li>
												<li class="divider"></li>
												<li><a href="<?php echo wp_logout_url( wp_guess_url() ); ?>"><i class="icon-remove"></i> Logout</a></li>
											</ul>
										</div>
										<!--<a class="btn logout" href="<?php echo wp_logout_url( wp_guess_url() ); ?>"><?php _e( 'Log Out', 'buddypress' ); ?></a>-->
									</li>
									<!--<?php //if ( bp_is_active( 'messages' ) ) : ?>
										<li id="site-wide-notices">
											<?php //bp_message_get_notices(); /* Site wide notices to all users */ ?>
										</li>
									<?php //endif; ?>-->
								<?php else : ?>
									<li id="sidebar-login-signup">
										<h2>Login / Signup</h2>
										<form name="login-form" id="sidebar-login-form" class="standard-form" action="<?php echo site_url( 'wp-login.php', 'login_post' ); ?>" method="post">
											<label><?php _e( 'Username', 'buddypress' ); ?><br />
											<input type="text" name="log" id="sidebar-user-login" class="input" value="<?php if ( isset( $user_login) ) echo esc_attr(stripslashes($user_login)); ?>" tabindex="97" /></label>
											<label><?php _e( 'Password', 'buddypress' ); ?><br />
											<input type="password" name="pwd" id="sidebar-user-pass" class="input" value="" tabindex="98" /></label>
											<p class="forgetmenot"><label class="checkbox"><input name="rememberme" type="checkbox" id="sidebar-rememberme" value="forever" tabindex="99" /> <?php _e( 'Remember Me', 'buddypress' ); ?></label></p>
											<?php do_action( 'bp_sidebar_login_form' ); ?>
											<input type="submit" name="wp-submit" id="sidebar-wp-submit" value="<?php _e( 'Log In', 'buddypress' ); ?>" tabindex="100" />
											<input type="hidden" name="testcookie" value="1" />
										</form>
										<?php if ( bp_get_signup_allowed() ) : ?>
											<h4>Need an Account?</h4>
											<p id="login-text">
												<?php printf( __( 'Please <a href="%s" title="Create an account">*create an account</a> to get started.', 'buddypress' ), bp_get_signup_page() ); ?>
												<p>*Use the following code as Registration Code: Rhino1000km</p>
											</p>
										<?php endif; ?>
									</li>
								<?php endif; ?>
								<?php /* Show forum tags on the forums directory */
								if ( bp_is_active( 'forums' ) && bp_is_forums_component() && bp_is_directory() ) : ?>
									<li id="forum-directory-tags" class="widget tags">
										<h2 class="widgettitle"><?php _e( 'Forum Topic Tags', 'buddypress' ); ?></h2>
										<div id="tag-text"><?php bp_forums_tag_heat_map(); ?></div>
									</li>
								<?php endif; ?>
								<?php if(function_exists('dynamic_sidebar') && dynamic_sidebar("Sidebar")) : else : ?><?php endif; ?>
							</ul>
						</aside>