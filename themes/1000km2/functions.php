<?php

register_nav_menus(array(
	'main' => 'Main Navigation',
	'secondary' => 'Secondary Navigation'
));

if (function_exists('register_sidebar')) {
    register_sidebar(array('name' => 'Sidebar',
        'id' => 'sidebar-1',
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
        'description' => 'The Default Sidebar widget area'
    ));
}

add_action('wp_enqueue_scripts', 'theme_load_scripts');
function theme_load_scripts(){
	wp_enqueue_script('bootstrap',get_template_directory_uri() . '/js/bootstrap.min.js',array('jquery'),false,true);
	wp_enqueue_script('css3-media-queries',get_template_directory_uri() . '/js/css3-mediaqueries.js',array(),false,true);
	wp_enqueue_script('dataTables',get_template_directory_uri() . '/js/jquery.dataTables.min.js',array(),false,true);
	wp_enqueue_script('site',get_template_directory_uri() . '/js/site.min.js',array(),false,true);
}
add_action('wp_enqueue_scripts', 'theme_load_style');
function theme_load_style(){
	wp_enqueue_style('bootstrap',get_template_directory_uri() . '/css/bootstrap.min.css',array(),false);
}

//include the walker class
include(dirname(__FILE__).'/bootstrap-walker.php');

function theme_pagination($pages = '', $range = 4){
    $showitems = ($range * 2)+1;
    global $paged;
    if(empty($paged)) $paged = 1;
    if($pages == ''){
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages){
            $pages = 1;
        }
    }
    if(1 != $pages){
        echo "<div class='pagination'><ul>";
        if($paged > 2 /*&& $paged > $range+1*/ && $showitems < $pages){
			echo "<li><a href='".get_pagenum_link(1)."'>First</a></li>";
		}else{
			echo "<li class='disabled'><a href='".get_pagenum_link(1)."'>First</a></li>";
		}
        if($paged > 1 && $showitems < $pages){
			echo "<li><a href='".get_pagenum_link($paged - 1)."'>Previous</a></li>";
		}else{
			echo "<li class='disabled'><a href='".get_pagenum_link($paged - 1)."'>Previous</a></li>";
		}
        for ($i=1; $i <= $pages; $i++) {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
                echo ($paged == $i)? "<li class='active'><span class='current'>".$i."</span></li>":"<li><a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a></li>";
            }
        }
        if ($paged < $pages && $showitems < $pages){
			echo "<li><a href='".get_pagenum_link($paged + 1)."'>Next</a></li>";
		}else{
			echo "<li class='disabled'><a href='".get_pagenum_link($paged + 1)."'>Next</a></li>";
		}
        if ($paged < $pages-1 /*&&  $paged+$range-1 < $pages*/ && $showitems < $pages){
			echo "<li><a href='".get_pagenum_link($pages)."'>Last</a></li>";
		}else{
			echo "<li class='disabled'><a href='".get_pagenum_link($pages)."'>Laast</a></li>";
		}
        echo "</ul></div>\n";
    }
}

add_theme_support( 'post-thumbnails' );