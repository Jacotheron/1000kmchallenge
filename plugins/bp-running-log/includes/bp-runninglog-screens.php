<?php
/* * ******************************************************************************
 * Screen Functions
 *
 * Screen functions are the controllers of BuddyPress. They will execute when their
 * specific URL is caught. They will first save or manipulate data using business
 * functions, then pass on the user to a template file.
 */

function bp_runninglog_directory_setup() {
    if (bp_is_runninglog_component() && !bp_current_action() && !bp_current_item()) {
        // This wrapper function sets the $bp->is_directory flag to true, which help other
        // content to display content properly on your directory.
        bp_update_is_directory(true, 'runninglog');

        // Add an action so that plugins can add content or modify behavior
        do_action('bp_runninglog_directory_setup');

        bp_core_load_template(apply_filters('runninglog_directory_template', 'runninglog/index'));
    }
}

add_action('bp_screens', 'bp_runninglog_directory_setup');

function bp_runninglog_stats() {
    global $bp;
    if (bp_is_runninglog_component() && bp_is_current_action('stats') && bp_is_action_variable('save_log', 0)) {

        check_admin_referer('bp_runninglog_savelog');

        $edit_id = (isset($bp->action_variables[1]) ? $bp->action_variables[1] : 0);

        $user_id = $bp->displayed_user->id;
        $date = array(
            'day' => (int) $_POST['eventdate_day'],
            'month' => (int) $_POST['eventdate_month'],
            'year' => (int) $_POST['eventdate_year']
        );
        $distance = $_POST['event_distance'];
        $event = $_POST['event_name'];
        $event_cutoff = $_POST['event_cutoff'];
        $event_town = $_POST['event_town'];
        $time = $_POST['event_time'];
        bp_runninglog_save_log($user_id, $event, $event_cutoff, $event_town, $date, $distance, $time, $edit_id);

        bp_core_add_message(__('Log Saved', 'bp-runninglog'));

        if ($edit_id > 0) {
            bp_core_redirect(bp_displayed_user_domain() . 'runninglog/logs');
        }
        bp_core_redirect(bp_displayed_user_domain() . 'runninglog');
    }

    if (bp_is_runninglog_component() && bp_is_current_action('stats') && bp_is_action_variable('import_log', 0)) {

        check_admin_referer('bp_runninglog_importlog');

        $user_id = $bp->displayed_user->id;

        $file = $_FILES['log-file'];
		//echo "<pre>";
        //var_dump($_FILES);
        //var_dump($_POST);
        //var_dump($_GET);
		//echo "</pre>";
        if ($file['error'] == 0) {
            //there is no error
            //$mime = array('text/comma-separated-values', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.ms-excel', 'application/vnd.msexcel', 'text/anytext');
            //$finfo = new finfo(FILEINFO_MIME);
            //$type = $finfo->file($file['tmp_name']);
            //$mime = substr($type, 0, strpos($type, ';'));

            if (/*pathinfo($file['name'], PATHINFO_EXTENSION) == 'csv' && */$file['size'] > 0 /* && in_array($file['type'], $mime) */) {
                //we have a legal file
                bp_runninglog_save_csv($user_id, $file['tmp_name']);
                bp_core_add_message(__('File Successfully Imported. ', 'bp-runninglog'));
            } else {
                bp_core_add_message(__('File Type not Allowed. ' . (
                                pathinfo($file['name'], PATHINFO_EXTENSION) != 'csv' ? pathinfo($file['name'], PATHINFO_EXTENSION) : (
                                        $file['size'] == 0 ? 'No File Size' : '') )
                                , 'bp-runninglog'), 'error');
            }
        } else {
			switch($file['error']){
				case 1:
					bp_core_add_message('The uploaded file exceeds the upload_max_filesize directive in php.ini.', 'error');
					break;
				case 2:
					bp_core_add_message('The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.', 'error');
					break;
				case 3:
					bp_core_add_message('The uploaded file was only partially uploaded.', 'error');
					break;
				case 4:
					bp_core_add_message('No file was uploaded. ', 'error');
					break;
				case 6:
					bp_core_add_message('Missing a temporary folder.', 'error');
					break;
				case 7:
					bp_core_add_message('Failed to write file to disk.', 'error');
					break;
				case 8:
					bp_core_add_message('A PHP extension stopped the file upload.', 'error');
					break;
				default:
					bp_core_add_message(__('Something went wrong with the upload', 'bp-runninglog'), 'error');
					break;
			}
            
        }
		@unlink($file['tmp_name']);
        bp_core_redirect(bp_displayed_user_domain() . 'runninglog');
    }

    do_action('bp_runninglog_stats');

    add_action('bp_template_title', 'bp_runninglog_stats_title');
    add_action('bp_template_content', 'bp_runninglog_stats_content');
    bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/plugins'));
}

function bp_runninglog_stats_title() {
    _e('Running Statistics', 'bp-runninglog');
}

function bp_runninglog_stats_content() {
    global $bp;

    $year_now = date('Y');
    $month_now = date('n');
    if (bp_is_runninglog_component() && bp_is_current_action('stats') && bp_is_action_variable('year', 0)) {
        $year = $bp->action_variables[1];
        $years = explode('-', $year);
        $year_1 = $years[0];
        $year_2 = $years[1];
        if ($month_now > 6) {
            $current_year = ($year_now) . '-' . ($year_now + 1);
        } else {
            $current_year = ($year_now - 1) . '-' . ($year_now);
        }
        $month_now = 0;
    } else {
        if ($month_now > 6) {
            $year_1 = $year_now;
            $year_2 = $year_now + 1;
        } else {
            $year_1 = $year_now - 1;
            $year_2 = $year_now;
        }
        $year = $year_1 . '-' . $year_2;
        $current_year = false; //we do not need it
    }
	
	//privacy
	if($bp->loggedin_user->id != $bp->displayed_user->id && !user_can($bp->loggedin_user->id, 'create_others_running_log')){
		//we have established that the user is not viewing their own log and it is not an admin
		//now lets see their privacy settings
		$privacy = bp_get_profile_field_data('field=Running Log Privacy&user_id=' . $bp->displayed_user->id);
		if(($privacy == 'Friends Only' && !friends_check_friendship($bp->displayed_user->id,$bp->loggedin_user->id)) || $privacy == 'Private'){ //they are not friends
			?>
			<h4>Welcome to my Running Stats</h4>
			<div class="alert alert-box">
				<h5>Access Denied</h5>
				<?php echo $bp->displayed_user->userdata->display_name; ?> have denied access to their running log.
			</div>
			<p>No data to display.</p>
			<?php
			return;
		}
	}
	
    $prev_year = ($year_1 - 1) . '-' . ($year_1);
    if (($year_1 - 1) < 1990) {//this was before it was started
        $prev_year = false;
    }
    $next_year = ($year_2) . '-' . ($year_2 + 1);
    if ($year_2 > date('Y')) {//this is in the future
        $next_year = false;
    }
    $stats = bp_runninglog_get_stats($year, $bp->displayed_user->id);

    $columns = count($stats[0]);

    $send_link = wp_nonce_url($bp->displayed_user->domain . $bp->current_component . '/stats/save_log', 'bp_runninglog_savelog');
    $import_link = wp_nonce_url($bp->displayed_user->domain . $bp->current_component . '/stats/import_log', 'bp_runninglog_importlog');
    if ($next_year) {
        $next_year_link = $bp->displayed_user->domain . $bp->current_component . '/stats/year/' . $next_year;
    }
    if ($prev_year) {
        $prev_year_link = $bp->displayed_user->domain . $bp->current_component . '/stats/year/' . $prev_year;
    }
    $current_year_link = false;
    if ($current_year) {
        $current_year_link = $bp->displayed_user->domain . $bp->current_component . '/stats/';
    }
    $comrades_1 = bp_runninglog_get_previous_comrades_dates($year_1);
    $comrades_2 = bp_runninglog_get_previous_comrades_dates($year_2);
    if (!$comrades_1) {
        //what is the default
        $comrades_1 = array(6, 1); //default date is 1 June
    }
    if (!$comrades_2) {
        $comrades_2 = $comrades_1; //use the previous year's date for exaclty 12 months by default
    }
    $months = array(
        1 => 'January',
        2 => 'February',
        3 => 'March',
        4 => 'April',
        5 => 'May',
        6 => 'June',
        7 => 'July',
        8 => 'August',
        9 => 'September',
        10 => 'October',
        11 => 'November',
        12 => 'December',
    );
    $comrades_1_month = $comrades_1[0];
    $comrades_1_day = $comrades_1[1];
    $comrades_2_month = $comrades_2[0];
    $comrades_2_day = $comrades_2[1];
    //if ($comrades_2_day == 1) {
    //$comrades_2_month - 1;
    //$comrades_2_day = 31; //31 May
    //}
    //$comrades_2_day -= 1;
    //$comrades_2_day += 1; //add another day as less than is used here
    $comrades_1_day += 1; //now the day after the comrades
    if ($comrades_1_day == 32) {//a month can not have 32 days
        $comrades_1_month += 1; //go to next month
        $comrades_1_day = 1; //start with the first day - this is used with greater_than
    }
    ?>
    <h4><?php _e('Welcome to my Running Stats', 'bp-runninglog') ?></h4>

    <table id="runninglog-stats" style="table-layout:fixed;">
        <tr class="stat-bar-container">
            <?php
            foreach ($stats[0] as $month_stats) {
                $percentage = round($month_stats['distance'] / $stats[1] * 100, 2);
                ?>
                <td>
                    <div class="stat-bar" style="height:<?php echo $percentage; ?>%"></div>
                </td>
            <?php } ?>
        </tr>
        <tr class="stats-months">
            <?php foreach ($stats[0] as $month_stats) { ?>
                <td><strong><?php echo $month_stats['month_name'] . ($month_now == $month_stats['month_number'] ? '*' : ''); ?></strong></td>
            <?php } ?>
        </tr>
        <tr class="stats-distances">
            <?php foreach ($stats[0] as $month_stats) { ?>
                <td><?php echo $month_stats['distance']; ?> KM</td>
            <?php } ?>
        </tr>
        <tr class="stats-year_start_end">
            <td colspan="3" class="stats-year_nav_prev"><?php echo "$comrades_1_day {$months[$comrades_1_month]} $year_1"; ?></td>
            <td colspan="<?php echo ($columns - 6); ?>" class="stats-year_nav_now">Day after Comrades to next Comrades</td>
            <td colspan="3" class="stats-year_nav_next"><?php echo "$comrades_2_day {$months[$comrades_2_month]} $year_2"; ?></td>
        </tr>
        <tr class="stats-year_nav">
            <td colspan="3" class="stats-year_nav_prev">
                <?php if ($prev_year) { ?>
                    <a href="<?php echo $prev_year_link; ?>">&laquo; <?php echo $prev_year; ?></a>
                <?php } ?>
            </td>
            <td colspan="<?php echo ($columns - 6); ?>" class="stats-year_nav_now">
                <span><?php echo $year; ?></span>
                <?php if ($current_year_link) { ?>
                    <br /><a href="<?php echo $current_year_link; ?>">&laquo; <?php echo $current_year ?> &raquo;</a>
                <?php } ?>
            </td>
            <td colspan="3" class="stats-year_nav_next">
                <?php if ($next_year) { ?>
                    <a href="<?php echo $next_year_link; ?>"><?php echo $next_year; ?> &raquo;</a>
                <?php } ?>
            </td>
        </tr>
    </table>

    <table id="runninglog-info">
        <tr>
            <th>Year Target</th>
            <td class="stat-target">
                <?php
                $target = (bp_runninglog_get_target_achieved($bp->displayed_user->id, $year));
                ?>
                <div class="stat-bar-container-horizontal">
                    <div class="stat-bar stat-bar-horizontal" style="width:<?php echo $target['percentage']; ?>%">
                        <span class="stat-bar-horizontal-percentage"><?php echo $target['percentage2']; ?>%</span>
                    </div>
                </div>
                <?php echo $target['completed'] . ' KM of ' . $target['target'] . " KM"; ?>
            </td>
        </tr>
        <tr>
            <th>Year Distance</th>
            <td><?php echo bp_runninglog_get_total_distance($year, null, $bp->displayed_user->id); ?> KM</td>
        </tr>
        <tr>
            <th>Lifetime Distance</th>
            <td><?php echo bp_runninglog_get_total_distance(null, null, $bp->displayed_user->id); ?> KM</td>
        </tr>
    </table>
    <?php
    if ($bp->displayed_user->id == $bp->loggedin_user->id || user_can($bp->loggedin_user->id, 'create_others_running_log')) {
        $year_now = date('Y');
        $month_now = date('n');
        $day_now = date('j');
        ?>
        <form action="<?php echo $import_link; ?>" class="pull-right" style="width:45%;" method="post" enctype="multipart/form-data">
            <div>
                <h4>Import Multiple Events</h4>
                <div class="insert-log-file">
                    <label for="log-file" style="display:block;">Select your Log file:</label>
                    <p><input type="file" name="log-file" id="log-file" /></p>
                </div>
                <div class="insert-log-button">
                    <p><button type="submit" class="btn">Import File</button></p>
					<input type="hidden" name="MAX_FILE_SIZE" value="10M" />
                </div>
                <div class="description">
                    <p>The file needs to be in the "Comma Separated Value" format (.csv).</p>
                    <p>Please make use of our <a href="http://www.1000kmchallenge.co.za/wp-content/uploads/2013/05/Runninglog-Import-Template.csv">template file</a>,
                        it can be edited using Excel or any other Spreadsheet software.</p>
                    <p>Using the Template:</p>
                    <ol>
                        <li>Open the file in Excel (or other Spreadsheet Software).</li>
                        <li>Leave the top line as is, this is required to identify the data.</li>
                        <li>There are 3 lines of sample data (they are there as a guide to how we need the information). Modify them, and add your own data (you may use any number of lines).</li>
                        <li>The following columns are optional (if you have the information): "Event Cutoff Time", "Event Town". For these, please leave the cell empty to leave out.</li>
                        <li>Save the file, making sure it is as a 'Comma Separated Values' (also knows as 'Comma Delimited').</li>
                    </ol>
                    <p>Please keep the your file for future reference, as the uploaded file is deleted after the information was extracted.</p>
                </div>
            </div>
        </form>
        <form action="<?php echo $send_link; ?>" method="post" class="pull-left" style="width:54%;" id="insert-log-event">
            <div>
                <h4>Insert New Event Distance</h4>
                <div class="insert-log-event">
                    <label for="event_name">Event Name:</label>
                    <input type="text" name="event_name" class="input-large" value="" id="event_name" />
                </div>
                <div class="insert-log-event-town">
                    <label for="event_town">Event Town:</label>
                    <input type="text" name="event_town" value="" id="event_town" />
                </div>
                <div class="insert-log-date">
                    <label for="eventdate_month">Event Date:</label>
                    <select name="eventdate_day" id="eventdate_day">
                        <?php
                        for ($i = 1; $i < 32; ++$i) {
                            echo '<option value="' . $i . '"' . selected($day_now, $i, false) . '>' . $i . '</option>';
                        }
                        ?>
                    </select>
                    <select name="eventdate_month" id="eventdate_month">
                        <option value="01" <?php echo ($month_now == 1 ? 'selected="selected"' : ''); ?>>January</option>
                        <option value="02" <?php echo ($month_now == 2 ? 'selected="selected"' : ''); ?>>February</option>
                        <option value="03" <?php echo ($month_now == 3 ? 'selected="selected"' : ''); ?>>March</option>
                        <option value="04" <?php echo ($month_now == 4 ? 'selected="selected"' : ''); ?>>April</option>
                        <option value="05" <?php echo ($month_now == 5 ? 'selected="selected"' : ''); ?>>May</option>
                        <option value="06" <?php echo ($month_now == 6 ? 'selected="selected"' : ''); ?>>June</option>
                        <option value="07" <?php echo ($month_now == 7 ? 'selected="selected"' : ''); ?>>July</option>
                        <option value="08" <?php echo ($month_now == 8 ? 'selected="selected"' : ''); ?>>August</option>
                        <option value="09" <?php echo ($month_now == 9 ? 'selected="selected"' : ''); ?>>September</option>
                        <option value="10" <?php echo ($month_now == 10 ? 'selected="selected"' : ''); ?>>October</option>
                        <option value="11" <?php echo ($month_now == 11 ? 'selected="selected"' : ''); ?>>November</option>
                        <option value="12" <?php echo ($month_now == 12 ? 'selected="selected"' : ''); ?>>December</option>
                    </select>
                    <select name="eventdate_year" id="eventdate_year">
                        <?php
                        for ($i = (date('Y') + 1); $i > 1989; $i--) {
                            echo '<option value="' . $i . '"' . selected($year_now, $i, false) . '>' . $i . '</option>';
                        }
                        ?>
                    </select><!--<br />
                    <label for="challenge_year">Challenge Year:</label>
                    <select name="challenge_year" id="challenge_year">
                    <?php
                    for ($i = (date('Y')); $i > 1989; $i--) {
                        $range = $i . '-' . ($i + 1);
                        if ($month_now < 6) {
                            $use_year = $year_now - 1;
                        } else {
                            $use_year = $year_now;
                        }
                        echo '<option value="' . $i . '"' . selected($use_year, $i, false) . '>' . $range . '</option>';
                    }
                    ?>
                    </select>-->
                </div>
                <div class="insert-log-distance">
                    <label for="event_distance">Distance of Race Completed:</label>
                    <input type="text" name="event_distance" value="15.00" id="event_distance" /> KM
                </div>
                <div class="insert-log-time">
                    <label for="event_time">Event Official Time:</label>
                    <input type="text" name="event_time" value="00:00:00" id="event_time" /> HH:MM:SS
                </div>
                <div class="insert-log-event-cutoff">
                    <label for="event_cutoff">Event Cut-off Time:</label>
                    <input type="text" name="event_cutoff" value="" id="event_cutoff" />
                </div>
                <div class="insert-log-button">
                    <button type="submit" class="btn">Save Event</button>
                </div>
            </div>
        </form>
        <p style="clear:both;">&nbsp;</p>
        <?php
    }
}

function bp_runninglog_logs() {
    global $bp;
    if (bp_is_runninglog_component() && bp_is_current_action('logs') && bp_is_action_variable('delete_log', 0)) {
        $delete_id = (int) $bp->action_variables[1];
        $user_id = $bp->displayed_user->id;
        check_admin_referer('bp_runninglog_deletelog_' . $delete_id);

        bp_runninglog_delete_log($user_id, $delete_id);
        bp_core_add_message(__('Log Entry Deleted', 'bp-runninglog'));

        bp_core_redirect(bp_displayed_user_domain() . 'runninglog/logs');
    }

    do_action('bp_runninglog_logs');

    add_action('bp_template_title', 'bp_runninglog_logs_title');
    add_action('bp_template_content', 'bp_runninglog_logs_content');
    bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/plugins'));
}

function bp_runninglog_logs_title() {
    _e('Running Logs', 'bp-runninglog');
}

function bp_runninglog_logs_content() {
    global $bp;

    if (bp_is_runninglog_component() && bp_is_current_action('logs') && bp_is_action_variable('edit_log', 0)) {
        $edit_id = (int) $bp->action_variables[1];
        //$user_id = $bp->displayed_user->id;
        check_admin_referer('bp_runninglog_editlog_' . $edit_id);


        $send_link = wp_nonce_url($bp->displayed_user->domain . $bp->current_component . '/stats/save_log/' . $edit_id, 'bp_runninglog_savelog');

        $log = bp_runninglog_get_log($edit_id)[0];
        ?>
        <form action="<?php echo $send_link; ?>" method="post" id="insert-log-event">
            <div>
                <h4>Edit Log</h4>
                <div class="insert-log-event">
                    <label for="event_name">Event Name:</label>
                    <input type="text" name="event_name" value="<?php echo $log->event; ?>" id="event_name" />
                </div>
                <div class="insert-log-event-town">
                    <label for="event_town">Event Town:</label>
                    <input type="text" name="event_town" value="<?php echo $log->event_town; ?>" id="event_town" />
                </div>
                <div class="insert-log-date">
                    <label for="eventdate_month">Event Date:</label>
                    <select name="eventdate_day" id="eventdate_day">
                        <?php
                        for ($i = 1; $i < 32; ++$i) {
                            echo '<option value="' . $i . '"' . selected($log->date_day, $i, false) . '>' . $i . '</option>';
                        }
                        ?>
                    </select>
                    <select name="eventdate_month" id="eventdate_month">
                        <option value="01" <?php echo ($log->date_month == 1 ? 'selected="selected"' : ''); ?>>January</option>
                        <option value="02" <?php echo ($log->date_month == 2 ? 'selected="selected"' : ''); ?>>February</option>
                        <option value="03" <?php echo ($log->date_month == 3 ? 'selected="selected"' : ''); ?>>March</option>
                        <option value="04" <?php echo ($log->date_month == 4 ? 'selected="selected"' : ''); ?>>April</option>
                        <option value="05" <?php echo ($log->date_month == 5 ? 'selected="selected"' : ''); ?>>May</option>
                        <option value="06" <?php echo ($log->date_month == 6 ? 'selected="selected"' : ''); ?>>June</option>
                        <option value="07" <?php echo ($log->date_month == 7 ? 'selected="selected"' : ''); ?>>July</option>
                        <option value="08" <?php echo ($log->date_month == 8 ? 'selected="selected"' : ''); ?>>August</option>
                        <option value="09" <?php echo ($log->date_month == 9 ? 'selected="selected"' : ''); ?>>September</option>
                        <option value="10" <?php echo ($log->date_month == 10 ? 'selected="selected"' : ''); ?>>October</option>
                        <option value="11" <?php echo ($log->date_month == 11 ? 'selected="selected"' : ''); ?>>November</option>
                        <option value="12" <?php echo ($log->date_month == 12 ? 'selected="selected"' : ''); ?>>December</option>
                    </select>
                    <select name="eventdate_year" id="eventdate_year">
                        <?php
                        for ($i = (date('Y') + 1); $i > 1989; $i--) {
                            echo '<option value="' . $i . '"' . selected($log->date_year, $i, false) . '>' . $i . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="insert-log-distance">
                    <label for="event_distance">Distance of Race Completed:</label>
                    <input type="text" name="event_distance" value="<?php echo $log->distance; ?>" id="event_distance" /> KM
                </div>
                <div class="insert-log-time">
                    <label for="event_time">Event Official Time:</label>
                    <input type="text" name="event_time" value="<?php echo $log->time; ?>" id="event_time" /> HH:MM:SS
                </div>
                <div class="insert-log-event-cutoff">
                    <label for="event_cutoff">Event Cut-off Time:</label>
                    <input type="text" name="event_cutoff" value="<?php echo $log->event_cutoff; ?>" id="event_cutoff" />
                </div>
                <div class="insert-log-button">
                    <button type="submit" class="">Update Event</button>
                </div>
            </div>
        </form>
        <?php
        return;
    }
    $months = array(
        1 => 'January',
        2 => 'February',
        3 => 'March',
        4 => 'April',
        5 => 'May',
        6 => 'June',
        7 => 'July',
        8 => 'August',
        9 => 'September',
        10 => 'October',
        11 => 'November',
        12 => 'December',
    );
    $logs = bp_runninglog_get_logs($bp->displayed_user->id);
    ?>
    <h4><?php _e('Edit My Running Logs', 'bp-runninglog') ?></h4>
    <p><?php _e('Here you can edit your running logs. This page is only accessible for the profile owner and Admins.', 'bp-runninglog') ?></p>

    <?php if ($logs) { ?>
        <div id="ajax_message"></div>
        <table id="runninglog_logs" cellspacing="0" cellpadding="0" border="0" class="table table-bordered table-striped dataTable table-condensed">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Date</th>
                    <th>Event</th>
                    <th>Distance</th>
                    <th>Edit / Delete</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Date</th>
                    <th>Date</th>
                    <th>Event</th>
                    <th>Distance</th>
                    <th>Edit / Delete</th>
                </tr>
            </tfoot>
            <tbody>
                <?php
                $odd = true;
                $previous_date = array('year' => 9999);
                foreach ($logs as $log) {
                    $delete_link = wp_nonce_url($bp->displayed_user->domain . $bp->current_component . '/logs/delete_log/' . $log->id, 'bp_runninglog_deletelog_' . $log->id);
                    $edit_link = wp_nonce_url($bp->displayed_user->domain . $bp->current_component . '/logs/edit_log/' . $log->id, 'bp_runninglog_editlog_' . $log->id);
                    if ($odd == true) {
                        $tr_class = 'odd';
                        $odd = false;
                    } else {
                        $tr_class = 'even';
                        $odd = true;
                    }
                    if ($previous_date && ($previous_date['year'] > $log->date_year)) {
                        echo '<tr class="' . $tr_class . '"><td>' . $log->date_year . '-99-99</td><td class="divider"><strong>' . $log->date_year . '</strong></td><td></td><td></td><td></td></tr>';
                        if ($odd == true) {
                            $tr_class = 'odd';
                            $odd = false;
                        } else {
                            $tr_class = 'even';
                            $odd = true;
                        }
                    }
                    $time_interrim = explode(':', $log->time);
                    if (count($time_interrim) == 3) {
                        //we have hours, minutes and seconds
                        $additional_min = (int) $time_interrim[0] * 60;
                        if ((int) $additional_min[2] > 29) {
                            $additional_min + 1; //round the more than half second up
                        }
                        $min = (int) $time_interrim[1] + $additional_min;
                    }
                    $pace = round($min / $log->distance, 2);
                    ?>
                    <tr id="<?php echo 'log_id-' . $log->id . '-' . $bp->displayed_user->id; ?>" class="<?php echo $tr_class; ?>">
                        <td class="logs_date1"><?php echo $log->date_year . '-' . $log->date_month . '-' . $log->date_day; ?></td>
                        <td class="logs_date"><?php echo $log->date_day . ' ' . $months[(int) $log->date_month] . ' ' . $log->date_year; ?></td>
                        <td class="logs_event"><?php echo $log->event; ?><br /><?php echo $log->event_cutoff; ?> - <?php echo $log->event_town; ?></td>
                        <td class="logs_distance"><?php echo $log->distance; ?> KM - <?php echo $log->time; ?><br /><?php echo $pace; ?> Min/KM</td>
                        <td class="logs_delete">
                            <a href="<?php echo $edit_link; ?>" class="btn btn-warning">Edit</a>
                            <a href="<?php echo $delete_link; ?>" class="btn btn-danger confirm delete_runninglog">Delete</a>
                        </td>
                    </tr>
                    <?php
                    $previous_date = array('year' => $log->date_year, 'month' => $log->date_month);
                }
                ?>
            </tbody>
        </table>
        <?php
    }
}

function bp_traininglog_stats() {
    global $bp;

    if (bp_is_runninglog_component() && bp_is_current_action('training') && bp_is_action_variable('save_log', 0)) {
        check_admin_referer('bp_traininglog_savelog');

        $edit_id = (isset($bp->action_variables[1]) ? (int) $bp->action_variables[1] : 0);

        $user_id = $bp->displayed_user->id;
        $date = array(
            'day' => (int) $_POST['eventdate_day'],
            'month' => (int) $_POST['eventdate_month'],
            'year' => (int) $_POST['eventdate_year']
        );
        $type = isset($_POST['event_type']) ? $_POST['event_type'] : 'event';
        $distance = round((float) $_POST['event_distance'], 2);
        $time = $_POST['event_time'];

        bp_traininglog_save_log($user_id, $date, $distance, $time, $type, $edit_id);

        bp_core_add_message(__('Log Saved', 'bp-runninglog'));

        if ($edit_id > 0) {
            bp_core_redirect(bp_displayed_user_domain() . 'runninglog/traininglogs');
        }
        bp_core_redirect(bp_displayed_user_domain() . 'runninglog/training');
    }

    do_action('bp_traininglog_stats');

    add_action('bp_template_title', 'bp_traininglog_stats_title');
    add_action('bp_template_content', 'bp_traininglog_stats_content');
    bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/plugins'));
}

function bp_traininglog_stats_title() {
    _e('My Training Log', 'bp-runninglog');
}

function bp_traininglog_stats_content() {
    global $bp;

    $year_now = date('Y');
    $month_now = date('n');
    if (bp_is_runninglog_component() && bp_is_current_action('training') && bp_is_action_variable('year', 0)) {
        $year = $bp->action_variables[1];
        $years = explode('-', $year);
        $year_1 = $years[0];
        $year_2 = $years[1];
        if ($month_now > 6) {
            $current_year = ($year_now) . '-' . ($year_now + 1);
        } else {
            $current_year = ($year_now - 1) . '-' . ($year_now);
        }
        $month_now = 0;
    } else {
        if ($month_now > 6) {
            $year_1 = $year_now;
            $year_2 = $year_now + 1;
        } else {
            $year_1 = $year_now - 1;
            $year_2 = $year_now;
        }
        $year = $year_1 . '-' . $year_2;
        $current_year = false; //we do not need it
    }
	
	//privacy
	if($bp->loggedin_user->id != $bp->displayed_user->id && !user_can($bp->loggedin_user->id, 'create_others_running_log')){
		//we have established that the user is not viewing their own log and it is not an admin
		//now lets see their privacy settings
		$privacy = bp_get_profile_field_data('field=Running Log Privacy&user_id=' . $bp->displayed_user->id);
		if(($privacy == 'Friends Only' && !friends_check_friendship($bp->displayed_user->id,$bp->loggedin_user->id)) || $privacy == 'Private'){ //they are not friends
			?>
			<h4>Welcome to my Training Log</h4>
			<div class="alert alert-box">
				<h5>Access Denied</h5>
				<?php echo $bp->displayed_user->userdata->display_name; ?> have denied access to their training log.
			</div>
			<p>No data to display.</p>
			<?php
			return;
		}
	}
	
    $prev_year = ($year_1 - 1) . '-' . ($year_1);
    $next_year = ($year_2) . '-' . ($year_2 + 1);
    $stats = bp_traininglog_get_stats($year, $bp->displayed_user->id);

    $send_link = wp_nonce_url($bp->displayed_user->domain . $bp->current_component . '/training/save_log', 'bp_traininglog_savelog');
    $next_year_link = $bp->displayed_user->domain . $bp->current_component . '/training/year/' . $next_year;
    $prev_year_link = $bp->displayed_user->domain . $bp->current_component . '/training/year/' . $prev_year;
    $current_year_link = false;
    if ($current_year) {
        $current_year_link = $bp->displayed_user->domain . $bp->current_component . '/training/';
    }
    ?>
    <h4><?php _e('Welcome to my Training Log', 'bp-runninglog') ?></h4>

    <table id="traininglog-stats">
        <tr class="stat-bar-container">
            <?php
            foreach ($stats[0] as $month_stats) {
                $percentage = round($month_stats['distance'] / $stats[1] * 100, 2);
                ?>
                <td>
                    <div class="stat-bar" style="height:<?php echo $percentage; ?>%"></div>
                </td>
            <?php } ?>
        </tr>
        <tr class="stats-months">
            <?php foreach ($stats[0] as $month_stats) { ?>
                <td><strong><?php echo $month_stats['month_name'] . ($month_now == $month_stats['month_number'] ? '*' : ''); ?></strong></td>
            <?php } ?>
        </tr>
        <tr class="stats-distances">
            <?php foreach ($stats[0] as $month_stats) { ?>
                <td><?php echo $month_stats['distance']; ?> KM</td>
            <?php } ?>
        </tr>
        <tr class="stats-year_nav">
            <td colspan="3" class="stats-year_nav_prev">
                <a href="<?php echo $prev_year_link; ?>">&laquo; <?php echo $prev_year; ?></a>
            </td>
            <td colspan="6" class="stats-year_nav_now">
                <span><?php echo $year; ?></span>
                <?php if ($current_year_link) { ?>
                    <br /><a href="<?php echo $current_year_link; ?>">&laquo; <?php echo $current_year ?> &raquo;</a>
                <?php } ?>
            </td>
            <td colspan="3" class="stats-year_nav_next">
                <a href="<?php echo $next_year_link; ?>"><?php echo $next_year; ?> &raquo;</a>
            </td>
        </tr>
    </table>

    <table id="traininglog-info">
        <tr>
            <th>Year Training Distance</th>
            <td><?php echo bp_traininglog_get_total_distance($year, null, $bp->displayed_user->id); ?> KM</td>
        </tr>
        <tr>
            <th>Lifetime Training Distance</th>
            <td><?php echo bp_traininglog_get_total_distance(null, null, $bp->displayed_user->id); ?> KM</td>
        </tr>
    </table>
    <?php
    if ($bp->displayed_user->id == $bp->loggedin_user->id) {
        $year_now = date('Y');
        $month_now = date('n');
        $day_now = date('j');
        ?>
        <form action="<?php echo $send_link; ?>" method="post" id="insert-log-event">
            <div>
                <h4>Insert New Training</h4>
                <div class="insert-log-date">
                    <label for="eventdate_month">Date:</label>
                    <select name="eventdate_day" id="eventdate_day">
                        <?php
                        for ($i = 1; $i < 32; ++$i) {
                            echo '<option value="' . $i . '"' . selected($day_now, $i, false) . '>' . $i . '</option>';
                        }
                        ?>
                    </select>
                    <select name="eventdate_month" id="eventdate_month">
                        <option value="01" <?php echo ($month_now == 1 ? 'selected="selected"' : ''); ?>>January</option>
                        <option value="02" <?php echo ($month_now == 2 ? 'selected="selected"' : ''); ?>>February</option>
                        <option value="03" <?php echo ($month_now == 3 ? 'selected="selected"' : ''); ?>>March</option>
                        <option value="04" <?php echo ($month_now == 4 ? 'selected="selected"' : ''); ?>>April</option>
                        <option value="05" <?php echo ($month_now == 5 ? 'selected="selected"' : ''); ?>>May</option>
                        <option value="06" <?php echo ($month_now == 6 ? 'selected="selected"' : ''); ?>>June</option>
                        <option value="07" <?php echo ($month_now == 7 ? 'selected="selected"' : ''); ?>>July</option>
                        <option value="08" <?php echo ($month_now == 8 ? 'selected="selected"' : ''); ?>>August</option>
                        <option value="09" <?php echo ($month_now == 9 ? 'selected="selected"' : ''); ?>>September</option>
                        <option value="10" <?php echo ($month_now == 10 ? 'selected="selected"' : ''); ?>>October</option>
                        <option value="11" <?php echo ($month_now == 11 ? 'selected="selected"' : ''); ?>>November</option>
                        <option value="12" <?php echo ($month_now == 12 ? 'selected="selected"' : ''); ?>>December</option>
                    </select>
                    <select name="eventdate_year" id="eventdate_year">
                        <?php
                        for ($i = (date('Y') + 1); $i > 1989; $i--) {
                            echo '<option value="' . $i . '"' . selected($year_now, $i, false) . '>' . $i . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="insert-log-distance">
                    <label for="event_distance">Distance of Training:</label>
                    <input type="text" name="event_distance" value="15.00" id="event_distance" /> KM
                </div>
                <div class="insert-log-time">
                    <label for="event_time">Training Time:</label>
                    <input type="text" name="event_time" value="00:00:00" id="event_time" /> HH:MM:SS
                </div>
                <div class="insert-log-button">
                    <button type="submit" class="">Save Event</button>
                </div>
            </div>
        </form>
        <?php
    }
}

function bp_traininglog_logs() {
    global $bp;

    if (bp_is_runninglog_component() && bp_is_current_action('traininglogs') && bp_is_action_variable('delete_log', 0)) {
        $delete_id = (int) $bp->action_variables[1];
        $user_id = $bp->displayed_user->id;
        check_admin_referer('bp_traininglog_deletelog_' . $delete_id);

        bp_traininglog_delete_log($user_id, $delete_id);

        bp_core_add_message(__('Log Deleted', 'bp-runninglog'));

        bp_core_redirect(bp_displayed_user_domain() . 'runninglog/traininglogs');
    }

    do_action('bp_traininglog_logs');

    add_action('bp_template_title', 'bp_traininglog_logs_title');
    add_action('bp_template_content', 'bp_traininglog_logs_content');
    bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/plugins'));
}

function bp_traininglog_logs_title() {
    _e('Training Logs Edit', 'bp-runninglog');
}

function bp_traininglog_logs_content() {
    global $bp;

    if (bp_is_runninglog_component() && bp_is_current_action('traininglogs') && bp_is_action_variable('edit_log', 0)) {
        $edit_id = (int) $bp->action_variables[1];
        //$user_id = $bp->displayed_user->id;
        check_admin_referer('bp_traininglog_editlog_' . $edit_id);

        $send_link = wp_nonce_url($bp->displayed_user->domain . $bp->current_component . '/training/save_log/' . $edit_id, 'bp_traininglog_savelog');

        $log = bp_traininglog_get_log($edit_id)[0];
        $year_now = $log->date_year;
        $month_now = $log->date_month;
        $day_now = $log->date_day;
        ?>
        <form action="<?php echo $send_link; ?>" method="post" id="insert-log-event">
            <div>
                <h4>Edit Training Log</h4>
                <div class="insert-log-date">
                    <label for="eventdate_month">Date:</label>
                    <select name="eventdate_day" id="eventdate_day">
                        <?php
                        for ($i = 1; $i < 32; ++$i) {
                            echo '<option value="' . $i . '"' . selected($day_now, $i, false) . '>' . $i . '</option>';
                        }
                        ?>
                    </select>
                    <select name="eventdate_month" id="eventdate_month">
                        <option value="01" <?php echo ($month_now == 1 ? 'selected="selected"' : ''); ?>>January</option>
                        <option value="02" <?php echo ($month_now == 2 ? 'selected="selected"' : ''); ?>>February</option>
                        <option value="03" <?php echo ($month_now == 3 ? 'selected="selected"' : ''); ?>>March</option>
                        <option value="04" <?php echo ($month_now == 4 ? 'selected="selected"' : ''); ?>>April</option>
                        <option value="05" <?php echo ($month_now == 5 ? 'selected="selected"' : ''); ?>>May</option>
                        <option value="06" <?php echo ($month_now == 6 ? 'selected="selected"' : ''); ?>>June</option>
                        <option value="07" <?php echo ($month_now == 7 ? 'selected="selected"' : ''); ?>>July</option>
                        <option value="08" <?php echo ($month_now == 8 ? 'selected="selected"' : ''); ?>>August</option>
                        <option value="09" <?php echo ($month_now == 9 ? 'selected="selected"' : ''); ?>>September</option>
                        <option value="10" <?php echo ($month_now == 10 ? 'selected="selected"' : ''); ?>>October</option>
                        <option value="11" <?php echo ($month_now == 11 ? 'selected="selected"' : ''); ?>>November</option>
                        <option value="12" <?php echo ($month_now == 12 ? 'selected="selected"' : ''); ?>>December</option>
                    </select>
                    <select name="eventdate_year" id="eventdate_year">
                        <?php
                        for ($i = (date('Y') + 1); $i > 1989; $i--) {
                            echo '<option value="' . $i . '"' . selected($year_now, $i, false) . '>' . $i . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="insert-log-distance">
                    <label for="event_distance">Distance of Training:</label>
                    <input type="text" name="event_distance" value="<?php echo $log->distance; ?>" id="event_distance" /> KM
                </div>
                <div class="insert-log-time">
                    <label for="event_time">Training Time:</label>
                    <input type="text" name="event_time" value="<?php echo $log->time; ?>" id="event_time" /> HH:MM:SS
                </div>
                <div class="insert-log-button">
                    <button type="submit" class="">Update Event</button>
                </div>
            </div>
        </form>
        <?php
        return;
    }
    $months = array(
        1 => 'January',
        2 => 'February',
        3 => 'March',
        4 => 'April',
        5 => 'May',
        6 => 'June',
        7 => 'July',
        8 => 'August',
        9 => 'September',
        10 => 'October',
        11 => 'November',
        12 => 'December',
    );
    $logs = bp_traininglog_get_logs($bp->displayed_user->id);
    ?>
    <h4><?php _e('Edit My Training Logs', 'bp-runninglog') ?></h4>
    <p><?php _e('Here you can edit your training logs. This page is only accessible for the profile owner and Admins.', 'bp-runninglog') ?></p>

    <?php if ($logs) { ?>
        <table id="traininglog_logs" cellspacing="0" cellpadding="0" border="0" class="table table-bordered table-striped dataTable table-condensed">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Date</th>
                    <th>Distance</th>
                    <th>Time</th>
                    <th>Edit / Delete</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Date</th>
                    <th>Date</th>
                    <th>Distance</th>
                    <th>Time</th>
                    <th>Edit / Delete</th>
                </tr>
            </tfoot>
            <tbody>
                <?php
                $odd = true;
                foreach ($logs as $log) {
                    $delete_link = wp_nonce_url($bp->displayed_user->domain . $bp->current_component . '/traininglogs/delete_log/' . $log->id, 'bp_traininglog_deletelog_' . $log->id);
                    $edit_link = wp_nonce_url($bp->displayed_user->domain . $bp->current_component . '/traininglogs/edit_log/' . $log->id, 'bp_traininglog_editlog_' . $log->id);
                    if ($odd == true) {
                        $tr_class = 'odd';
                        $odd = false;
                    } else {
                        $tr_class = 'even';
                        $odd = true;
                    }
                    ?>
                    <tr class="<?php echo $tr_class; ?>">
                        <td class="logs_date1"><?php echo $log->date_year . '-' . $log->date_month . '-' . $log->date_day; ?></td>
                        <td class="logs_date"><?php echo $log->date_day . ' ' . $months[(int) $log->date_month] . ' ' . $log->date_year; ?></td>
                        <td class="logs_distance"><?php echo $log->distance; ?> KM</td>
                        <td class="logs_time"><?php echo $log->time; ?></td>
                        <td class="logs_delete">
                            <a href="<?php echo $edit_link; ?>" class="btn btn-warning">Edit</a>
                            <a href="<?php echo $delete_link; ?>" class="btn btn-danger confirm">Delete</a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php
    }
}

function bp_runninglog_awards() {
    //global $bp;
    do_action('bp_runninglog_awards');

    add_action('bp_template_title', 'bp_runninglog_awards_title');
    add_action('bp_template_content', 'bp_runninglog_awards_content');

    bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/plugins'));
}

function bp_runninglog_awards_title() {
    _e('Running Awards', 'bp-runninglog');
}

function bp_runninglog_awards_content() {
    global $bp;
    if (bp_is_runninglog_component() && bp_is_current_action('awards') && bp_is_action_variable('give_award', 0)) {
        check_admin_referer('bp_runninglog_giveaward');
        $award_id = $_POST['award_id'];
        $user_id = $bp->displayed_user->id;
        $reason = $_POST['reason'];
        //bp_runninglog_give_award($award_id, $user_id, $reason);
        //bp_core_redirect(bp_displayed_user_domain() . 'runninglog/awards');
    }
    if (bp_is_runninglog_component() && bp_is_current_action('awards') && bp_is_action_variable('take_award', 0)) {
        $delete_id = (int) $bp->action_variables[1];
        $user_id = $bp->displayed_user->id;
        check_admin_referer('bp_runninglog_deleteandtakeeaward_' . $delete_id);

        //bp_runninglog_take_award($delete_id, $user_id);
        //bp_core_redirect(bp_displayed_user_domain() . 'runninglog/awards');
    }
    ?>
    <h4><?php _e('Welcome to my Awards', 'bp-runninglog') ?></h4>
    <?php
    $awards = ''; //bp_runninglog_get_user_awards($bp->displayed_user->id, 'id');
    if (empty($awards)) {
        if (user_can($bp->loggedin_user->id, 'give_award')) {
            echo "<p>This user have not been awarded any Awards. You can award an award to them, if they deserve it.</p>";
        } elseif ($bp->loggedin_user->id == $bp->displayed_user->id) {
            echo "<p>You have not been awarded any Awards. Please continue trying, you are almost there.</p>";
        } else {
            echo "<p>This user have not received any awards.</p>";
        }
    } else {
        foreach ($awards as $award_id_object) {
            $award_id = (int) $award_id_object->award_id;
            $award = bp_runninglog_get_award($award_id);
            $thumbnail = bp_runninglog_get_award($award_id, 'thumbnail', array(100, 100), null);
            ?>
            <table id="awards-table">
                <tr>
                    <td><?php echo $thumbnail; ?></td>
                    <td>
                        <h5><?php echo $award->post_title; ?></h5>
                        <?php echo apply_filters('the_content', $award->post_content); ?>
                    </td>
                    <?php
                    if (user_can($bp->loggedin_user->id, 'take_award')) {
                        $delete_link = wp_nonce_url($bp->displayed_user->domain . $bp->current_component . '/awards/take_award/' . $award_id, 'bp_runninglog_deleteandtakeeaward_' . $award_id);
                        ?>
                        <td><a href="<?php echo $delete_link; ?>" class="btn btn-danger confirm">Delete</a></td>
                    <?php } ?>
                </tr>
            </table>
            <?php
        }
    }
    if (user_can($bp->loggedin_user->id, 'give_award')) {
        $award_strings = array(
            'Around The World' => array(
                'string' => 'AROUND THE WORLD ON THE EQUATOR TROPHY (40 075.16KM) (24 901.55MILES) - %1$s',
                'vars' => array(
                    1 => 'year_achieved'
                )
            ),
            'Record Distance' => array(
                'string' => '%1$s KM- %2$s %3$s RECORD FOR FURTHEST DISTANCE IN ONE CHALLENGE YEAR %4$s',
                'vars' => array(
                    1 => 'distance',
                    2 => 'gender_category',
                    3 => 'type_category',
                    4 => 'year_achieved'
                )
            ),
            'Medals' => array(
                'string' => '%1$s MEDAL - %2$s KM (%3$s)',
                'vars' => array(
                    1 => 'distance_colors',
                    2 => 'distance',
                    3 => 'year_achieved'
                )
            ),
            'Permanent Number' => array(
                'string' => 'PERMANENT NUMBER %1$s (%2$s CHALLENGES)',
                'vars' => array(
                    1 => 'user_permanent_number',
                    2 => 'challenges'
                )
            ),
            'Year Winner' => array(
                'string' => '%1$s OVERALL WINNER',
                'vars' => array(
                    1 => 'year_achieved'
                )
            )
        );
        $vars = array(
            'year_achieved' => array(
                'type' => 'range',
                'options' => array(
                    'start' => date('Y'),
                    'decrement' => 1,
                    'stop' => '1990'
                ),
                'label' => 'Year Achieved'
            ),
            'distance' => array(
                'type' => 'text',
                'default' => '',
                'label' => 'Distance'
            ),
            'gender_category' => array(
                'type' => 'select',
                'options' => array(
                    'Overall',
                    'Male',
                    'Female'
                ),
                'label' => 'Genders'
            ),
            'type_category' => array(
                'type' => 'select',
                'options' => array(
                    ' ',
                    'Runners',
                    'Walkers',
                    'Wheelchairs'
                ),
                'label' => 'Runner Type'
            ),
            'distance_colors' => array(
                'type' => 'select',
                'options' => array(
                    '500KM Small Bronze Runners',
                    '1000KM Bronze Runners',
                    '1609KM Silver Runners',
                    '2414KM Gold Runners',
                    '5023KM Platinum Runners',
                    ' -- -- ',
                    '500KM Bronze Walkers',
                    '805KM Silver Walkers',
                    '1207KM Gold Walkers',
                    '2512KM Platinum Walkers',
                ),
                'label' => 'Distance / Medals'
            ),
            'user_permanent_number' => array(
                'type' => 'text',
                'default' => '',
                'label' => 'Permanent Number'
            ),
            'challenges' => array(
                'type' => 'range',
                'options' => array(
                    'start' => '3',
                    'increment' => '3',
                    'stop' => (date('Y') - 1990 + 3)
                ),
                'label' => '# of Challenges'
            )
        );



        /* $award_list = get_posts(
          array(
          'numberposts' => -1,
          'post_type' => 'awards',
          'order' => 'ASC',
          'orderby' => 'title'
          )); */
        $send_link = wp_nonce_url($bp->displayed_user->domain . $bp->current_component . '/awards/give_award', 'bp_runninglog_giveaward');
        ?>
        <form id="awarding_awards" action="<?php echo $send_link; ?>" method="post">
            <div>
                <h4>Award <?php echo $bp->displayed_user->userdata->display_name; ?> with an Award</h4>
                <p>This form is currently not able to save any data.</p>
                <div class="award_awardtype">
                    <label for="award_awardtype">Select the Award Type</label>
                    <select name="award_awardtype" id="award_awardtype">
                        <option value="">-- Select Award --</option>
                        <?php foreach ($award_strings as $award_name => $award_info) { ?>
                            <option value="<?php echo $award_name; ?>"><?php echo $award_name; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <?php
                $fields_generated = array();
                foreach ($award_strings as $award_name => $award_info) {
                    foreach ($award_info['vars'] as $field) {
                        if (in_array($field, $fields_generated)) {
                            break;
                        }
                        ?>
                        <div id="award_<?php echo $field; ?>">
                            <label for="field_<?php echo $field; ?>"><?php echo $vars[$field]['label']; ?></label>
                            <?php
                            if ($vars[$field]['type'] == 'range') {
                                ?>
                                <select name="field_<?php echo $field; ?>" id="field_<?php echo $field; ?>">
                                    <?php
                                    //now determine what should be done with $i
                                    $up_down = 'up';
                                    if (isset($vars[$field]['options']['decrement'])) {
                                        $j = 0 - (int) $vars[$field]['options']['decrement'];
                                        $up_down = 'down';
                                    }
                                    if (isset($vars[$field]['options']['increment'])) {
                                        $j = (int) $vars[$field]['options']['increment'];
                                        $up_down = 'up';
                                    }
                                    for ($i = (int) $vars[$field]['options']['start']; range_possible_end($i, $up_down, $vars[$field]['options']['stop']); $i = range_iteration($i, $j)) {
                                        if ($field == 'year_achieved') {
                                            echo "<option>$i-" . ($i + 1) . "</option>";
                                        } else {
                                            echo "<option>$i</option>";
                                        }
                                    }
                                    ?>
                                </select>
                                <?php
                            } elseif ($vars[$field]['type'] == 'text') {
                                echo "<input type='text' name='field_$field' id='field_$field' value='{$vars[$field]['default']}' />";
                            } elseif ($vars[$field]['type'] == 'select') {
                                ?>
                                <select name="field_<?php echo $field; ?>" id="field_<?php echo $field; ?>">
                                    <?php
                                    foreach ($vars[$field]['options'] as $option) {
                                        echo "<option>$option</option>";
                                    }
                                    ?>
                                </select>
                                <?php
                            }
                            $fields_generated[] = $field;
                            ?>
                        </div>
                        <?php
                    }
                }
                ?>
                <div class="award_buttons">
                    <button type="submit" class="btn" style="margin-top:20px;">Give Award</button>
                </div>
            </div>
            <script>
                jQuery(document).ready(function($) {
                    var year_achieved = jQuery('#award_year_achieved');
                    var distance = jQuery('#award_distance');
                    var gender_category = jQuery('#award_gender_category');
                    var type_category = jQuery('#award_type_category');
                    var distance_colors = jQuery('#award_distance_colors');
                    var user_permanent_number = jQuery('#award_user_permanent_number');
                    var challenges = jQuery('#award_challenges');
                    function hide_fields() {
                        year_achieved.hide(1000);
                        distance.hide(1000);
                        gender_category.hide(1000);
                        type_category.hide(1000);
                        distance_colors.hide(1000);
                        user_permanent_number.hide(1000);
                        challenges.hide(1000);
                    }
                    hide_fields();

                    jQuery('#award_awardtype').change(function() {
                        var selectedValue = $(this).val();
                        if (selectedValue === 'Around The World') {
                            hide_fields();
                            year_achieved.show(1000);
                        } else if (selectedValue === 'Record Distance') {
                            hide_fields();
                            year_achieved.show(1000);
                            distance.show(1000);
                            gender_category.show(1000);
                            type_category.show(1000);
                        } else if (selectedValue === 'Medals') {
                            hide_fields();
                            year_achieved.show(1000);
                            distance.show(1000);
                            distance_colors.show(1000);
                        } else if (selectedValue === 'Permanent Number') {
                            hide_fields();
                            user_permanent_number.show(1000);
                            challenges.show(1000);
                        } else if (selectedValue === 'Year Winner') {
                            hide_fields();
                            year_achieved.show(1000);
                        } else {
                            hide_fields();
                        }
                    });
                });
            </script>
        </form>
        <?php
    }
}

#EOF