<?php

function bp_runninglog_load_template_filter($found_template, $templates) {
//global $bp;

    return $found_template;

    foreach ((array) $templates as $template) {
        if (file_exists(STYLESHEETPATH . '/' . $template))
            $filtered_templates[] = STYLESHEETPATH . '/' . $template;
        else
            $filtered_templates[] = dirname(__FILE__) . '/templates/' . $template;
    }

    $found_template = $filtered_templates[0];

    return apply_filters('bp_runninglog_load_template_filter', $found_template);
}

add_filter('bp_located_template', 'bp_runninglog_load_template_filter', 10, 2);

function bp_runninglog_save_log($user_id, $event, $event_cutoff, $event_town, $date, $distance, $time = 0, $log_id = 0, $surpress_activity = false) {
    global $bp;
    global $wpdb;

    $day = $date['day'];
    $month = $date['month'];
    $year = $date['year'];
    //$ch_year = $date['ch_year'];

    $months = array(
        1 => 'January',
        2 => 'February',
        3 => 'March',
        4 => 'April',
        5 => 'May',
        6 => 'June',
        7 => 'July',
        8 => 'August',
        9 => 'September',
        10 => 'October',
        11 => 'November',
        12 => 'December',
    );

    $human_month = $months[(int) $month];
    $human_date = $day . ' ' . $human_month . ($year == date('Y') ? '' : ' ' . $year); //only display Year if other year than present

    $user_link = bp_core_get_userlink($bp->loggedin_user->id);
    if ($user_id != $bp->loggedin_user->id) {//they do not match
        if (!user_can($bp->loggedin_user->id, 'create_others_running_log')) {
            return false; //the user can not create a log for another user
        }
        $admin_link = $user_link; //the person posting the distance to the user
        $user_link = bp_core_get_userlink($user_id);
        $action = sprintf(__('%1s updated %2s\'s <strong>Running Log</strong>!', 'bp-runninglog'), $admin_link, $user_link);
        $content = sprintf(__('On <strong>%1s</strong> at the <strong>%2s</strong>, I ran <strong>%3skm</strong> in <strong>%4s</strong>!', 'bp-runninglog'), $human_date, $event, $distance, $time);
    } else {
        if (!user_can($user_id, 'create_running_log')) {
            return false; //this user may not even update his/her own running log
        }
        $action = sprintf(__('%1s updated their <strong>Running Log</strong>!', 'bp-runninglog'), $user_link);
        $content = sprintf(__('On <strong>%1s</strong> at the <strong>%2s</strong>, I ran <strong>%3skm</strong> with a total time of <strong>%4s</strong>!', 'bp-runninglog'), $human_date, $event, $distance, $time);
    }
    $table = $bp->table_prefix . "bp_runninglog";
    $data = array(
        'user_id' => $user_id,
        'date_year' => $year,
        'date_month' => $month,
        'date_day' => $day,
        'event' => $event,
        'event_cutoff' => $event_cutoff,
        'event_town' => $event_town,
        'time' => $time,
        'distance' => $distance
    );
    $format = array(
        '%d',
        '%s',
        '%s',
        '%s',
        '%s',
        '%s',
        '%s',
        '%s',
        '%s'
    );
    if ($log_id > 0) {
        $prepare = $wpdb->prepare("
                UPDATE $table SET
                    `date_year` = %2d,
                    `date_month` = %3d,
                    `date_day` = %4d,
                    `event` = '%5s',
                    `event_cutoff` = '%6s',
                    `event_town` = '%7s',
                    `time` = '%8s',
                    `distance` = '%9s'
                WHERE `id` = %10d LIMIT 1
                ", $year, $month, $day, $event, $event_cutoff, $event_town, $time, $distance, $log_id);
        $wpdb->query($prepare);
    } else {
        $wpdb->insert($table, $data, $format);

        if (!$surpress_activity) {
            bp_activity_add(array(
                'type' => 'add_runninglog',
                'action' => apply_filters('bp_runninglog_save_action', $action, $user_link),
                'content' => apply_filters('bp_runninglog_save_content', $content, $user_link, $date, $distance),
                'component' => 'runninglog',
                'user_id' => $user_id,
                'item_id' => 'runninglog_' . $wpdb->insert_id
            ));
        }
    }

    do_action('bp_runninglog_save_log', $bp->loggedin_user->id);

    return true;
}

function bp_traininglog_save_log($user_id, $date, $distance, $time = 0, $type = "event", $log_id = 0) {
    global $bp;
    global $wpdb;

    $day = $date['day'];
    $month = $date['month'];
    $year = $date['year'];

    $months = array(
        1 => 'January',
        2 => 'February',
        3 => 'March',
        4 => 'April',
        5 => 'May',
        6 => 'June',
        7 => 'July',
        8 => 'August',
        9 => 'September',
        10 => 'October',
        11 => 'November',
        12 => 'December',
    );

    $human_month = $months[(int) $month];
    $human_date = $day . ' ' . $human_month . ($year == date('Y') ? '' : ' ' . $year); //only display Year if other year than present

    $user_link = bp_core_get_userlink($bp->loggedin_user->id);
    if ($user_id != $bp->loggedin_user->id) {//they do not match
        return false; //the user can not create a log for another user
    }
    if (!user_can($user_id, 'create_running_log')) {
        return false; //this user may not even update his/her own running log
    }
    $action = sprintf(__('%1s updated their <strong>Training Log</strong>!', 'bp-runninglog'), $user_link);
    $content = sprintf(__('On <strong>%1s</strong>, I trained <strong>%2skm</strong> in <strong>%3s</strong>!', 'bp-runninglog'), $human_date, $distance, $time);

    $table = $bp->table_prefix . "bp_traininglog";
    $data = array(
        'user_id' => $user_id,
        'date_year' => $year,
        'date_month' => $month,
        'date_day' => $day,
        'type' => $type,
        'time' => $time,
        'distance' => $distance
    );
    $format = array(
        '%d',
        '%s',
        '%s',
        '%s',
        '%s',
        '%s',
        '%s'
    );
    if ($log_id > 0) {
        $wpdb->query($wpdb->prepare("
                UPDATE $table SET
                    `date_year` = %1d,
                    `date_month` = %2d,
                    `date_day` = %3d,
                    `time` = '%4s',
                    `distance` = '%5s'
                WHERE `id` = %6d LIMIT 1
                ", $year, $month, $day, $time, $distance, $log_id));
    } else {
        $wpdb->insert($table, $data, $format);

        bp_activity_add(array(
            'type' => 'add_traininglog',
            'action' => apply_filters('bp_traininglog_save_action', $action, $user_link),
            'content' => apply_filters('bp_traininglog_save_content', $content, $user_link, $date, $distance),
            'component' => 'traininglog',
            'user_id' => $user_id,
            'item_id' => 'traininglog_' . $wpdb->insert_id
        ));
    }
    do_action('bp_traininglog_save_log', $bp->loggedin_user->id);

    return true;
}

function bp_runninglog_delete_log($user_id, $log_id) {
    global $bp;
    global $wpdb;

    check_admin_referer('bp_runninglog_deletelog_' . $log_id);

    if ($user_id != $bp->loggedin_user->id) {//they do not match
        if (!user_can($bp->loggedin_user->id, 'delete_others_running_logs')) {
            return false; //the user can not create a log for another user
        }
    } else {
        if (!user_can($user_id, 'delete_own_running_logs')) {
            return false; //this user may not even update his/her own running log
        }
    }
    $table = $bp->table_prefix . "bp_runninglog";
    $wpdb->query(
            $wpdb->prepare(
                    "DELETE FROM `$table`
WHERE `user_id` = %1d
AND `id` = %2d
LIMIT 1
", $user_id, $log_id
            )
    );

    return true;
}

function bp_traininglog_delete_log($user_id, $log_id) {
    global $bp;
    global $wpdb;

    check_admin_referer('bp_traininglog_deletelog_' . $log_id);

    if ($user_id != $bp->loggedin_user->id) {//they do not match
        if (!user_can($bp->loggedin_user->id, 'delete_others_running_logs')) {
            return false; //the user can not create a log for another user
        }
    } else {
        if (!user_can($user_id, 'delete_own_running_logs')) {
            return false; //this user may not even update his/her own running log
        }
    }
    $table = $bp->table_prefix . "bp_traininglog";
    $wpdb->query(
            $wpdb->prepare(
                    "DELETE FROM `$table`
WHERE `user_id` = %1d
AND `id` = %2d
LIMIT 1
", $user_id, $log_id
            )
    );

    return true;
}

function bp_runninglog_get_total_distance($year = null, $month = null, $user_id = null, $years = null, $time = false) {
    global $bp;
    global $wpdb;

    $user = '';
    $date = '';
    $type = '';
    $where = '';
    $and = false;
    if ($user_id) {
        $user = " `user_id` = $user_id ";
    }

    if ($year) {
        $year_1 = (int) $year;
        $year_2 = $year_1 + 1;
        if (isset($years) && strlen($years) > 4) {//now a single year is considdered the start year
            $years = explode('-', $years);
            $years_1 = $years[0];
            $years_2 = $years[1];
        } else {
            $years_1 = (int) $year;
            $years_2 = $year_1 + 1;
        }
        $comrades_date_1 = bp_runninglog_get_previous_comrades_dates($year_1);
        $comrades_date_2 = bp_runninglog_get_previous_comrades_dates($year_2);
        //var_dump($comrades_date_1);
        //var_dump($comrades_date_2);
        if (!$comrades_date_1) {
            //what is the default
            $comrades_date_1 = array(6, 1); //default date is 1 June
        }
        if (!$comrades_date_2) {
            $comrades_date_2 = $comrades_date_1; //use the previous year's date for exaclty 12 months by default
        }






        //
        $offset = $comrades_date_1[0]; //this is the month of the comrades as start date
        $offset_day = $comrades_date_1[1]; //this is the day of comrades
        $limit = $comrades_date_2[0]; //the month of the next comdares as end date
        $limit_day = $comrades_date_2[1]; //this is the day before the next comrades
        /////////////////////////////////////////
        //if ($offset_day == 31) {
        //$offset = $offset + 1; //go to next month
        //$offset_day = 1;
        //}
        //////////////////////////////////////////
        $limit_day += 1; //add another day as less than is used here
        //$offset_day += 1; //now the day after the comrades
        //echo "Offeset: $offset_day; Limit: $limit_day";
        if ($offset_day == 32) {//a month can not have 32 days
            $offset = $offset + 1; //go to next month
            $offset_day = 0; //start with the first day - this is used with greater_than
        }
















        //if ($limit_day == 1) {
        //$limit = $limit - 1; //if the next comrades is on the last of the month, use beginning of next month
        //$limit_day = 32; //1 more than any months days to get all
        //}

        if ($month && $year) {
            $year = (int) $year;
            $month = (int) $month;
            $date = " `date_year` = $year && `date_month` = $month ";
            //echo "Month: $month; Offset: $offset; Limit: $limit; Year: $year; Year1: $year_1; Year2: $year_2";
            if ($month == $offset && $year == $years_1) {//this is the date of the start
                $date .= " && `date_day` >= $offset_day ";
            }
            if ($month == $limit && $year == $years_2) {//this is the date for the end
                $date .= " && `date_day` < $limit_day ";
            }
        } else {
            $year = (int) $year;
            $date = " ( " .
                    " (`date_year` = $year_1 && `date_month` > $offset) " . //these will cathch all in the normal months
                    " OR " .
                    " (`date_year` = $year_2 && `date_month` < $limit) " . //these will cathch all in the normal months
                    " OR " .
                    " (`date_year` = $year_1 && `date_month` = $offset && `date_day` > $offset_day) " . //these will cathch all in the other months
                    " OR " .
                    " (`date_year` = $year_2 && `date_month` = $limit && `date_day` < $limit_day) " . //these will cathch all in the other months
                    " ) ";
        }
    }
    /* if ($ch_year) {
      $year = (int) $year;
      $date = " `ch_year` = '$year' ";
      $type = " `type` = 'event' ";
      if ((int) $month && $year) {
      //$year is now a single year & month could potentially repeat
      //$comrades_date = bp_runninglog_get_previous_comrades_dates($year);
      //using the ch_year and month, should in theory give us the results we need
      $date .= " && `date_month` = $month "; //this will however merge the partial months data, but better than nothing
      }
      } else {
      if ($month && $year) {
      //select for a specific month of the year
      $year = (int) $year;
      $month = (int) $month;
      $date = " `date_year` = $year && `date_month` = $month ";
      $type = " `type` = 'event'";
      } elseif ($year) {
      //select for a specific year
      if (strlen($year) > 4) {
      //this is a compound year
      $years = explode('-', $year);
      $year1 = $years[0];
      $year2 = $years[1];
      if (strlen($year1) != strlen($year2) && strlen($year1) != 4) {
      return false; //this is no date format
      }
      $date = " (
      (`date_year` = $year1 && `date_month` > 5)
      OR
      (`date_year` = $year2 && `date_month` < 6) ) ";
      //a year is from July to June the next year
      } else {
      $year = (int) $year;
      $date = " `date_year` = $year ";
      }
      $type = " `type` = 'event'"; //the others are pre-time inserts, and should therefore not be used
      }
      } */
    if (!empty($user)) {
        $where .= $user;
        $and = true;
    }
    if (!empty($date)) {
        if ($and) {
            $where .= " && ";
        } else {
            $and = true;
        }
        $where .= $date;
    }
    if (!empty($type)) {
        if ($and) {
            $where .= " && ";
        } else {
            $and = true;
        }
        $where .= $type;
    }
    if (!$year && !$user_id) {
//get the total of all users
        $where = '';
    }
    $table = $bp->table_prefix . "bp_runninglog";
    $select = "SELECT SUM(`distance`) AS `total_distance` " . ($time ? ", SEC_TO_TIME( SUM( TIME_TO_SEC( `time` ) ) ) AS `total_time`" : "") . " FROM `$table` " .
            (!empty($where) ? ' WHERE ' . $where : '');

    //echo "<pre>" . $select . "</pre>";
    if ($user_id != $bp->loggedin_user->id && $user_id) {//they do not match
        if (!user_can($bp->loggedin_user->id, 'view_user_distance')) {
            return false; //the user can not view the user's total
        }
    }
    $total_time = "00:00:00";
    $total_distance = 0;
    $total = $wpdb->get_row($select);
    $return = '';
    if (!$total) {
        $total_distance = 0;
        $return = $total_distance;
        if ($time) {
            $return = array('distance' => $total_distance, 'time' => $total_time);
        }
    } else {
        $total_distance = $total->total_distance;
        if (!$total_distance) {
            $total_distance = 0;
        }
        $return = $total_distance;
        if ($time) {
            $total_time = $total->total_time;
            if (!$total_time) {
                $total_time = "00:00:00";
            }
            $return = array('distance' => $total_distance, 'time' => $total_time);
        }
    }
    //echo $total_distance . '<br/>';

    return $return;
}

function bp_traininglog_get_total_distance($year = null, $month = null, $user_id = null) {
    global $bp;
    global $wpdb;

    $user = '';
    $date = '';
    $type = '';
    $where = '';
    $and = false;
    if ($user_id) {
        $user = " `user_id` = $user_id ";
    }
    if ($month && $year) {
        $year = (int) $year;
        $month = (int) $month;
//select for a specific month of the year
        $date = " `date_year` = $year && `date_month` = $month ";
        $type = " `type` = 'event'";
    } elseif ($year) {
//select for a specific year
        if (strlen($year) > 4) {
//this is a compound year
            $years = explode('-', $year);
            $year1 = $years[0];
            $year2 = $years[1];
            if (strlen($year1) != strlen($year2) && strlen($year1) != 4) {
                return false; //this is no date format
            }
            $date = " (
(`date_year` = $year1 && `date_month` > 5)
OR
(`date_year` = $year2 && `date_month` < 6) ) ";
//a year is from July to June the next year
        } else {
            $year = (int) $year;
            $date = " `date_year` = $year ";
        }
        $type = " `type` = 'event'"; //the others are pre-time inserts, and should therefore not be used
    }
    if (!empty($user)) {
        $where .= $user;
        $and = true;
    }
    if (!empty($date)) {
        if ($and) {
            $where .= " && ";
        } else {
            $and = true;
        }
        $where .= $date;
    }
    if (!empty($type)) {
        if ($and) {
            $where .= " && ";
        } else {
            $and = true;
        }
        $where .= $type;
    }
    if (!$year && !$user_id) {
//get the total of all users
        $where = '';
    }
    $table = $bp->table_prefix . "bp_traininglog";
    $select = "SELECT SUM(`distance`) AS `total_distance` FROM `$table` " .
            (!empty($where) ? ' WHERE ' . $where : '');

    if ($user_id != $bp->loggedin_user->id && $user_id) {//they do not match
        if (!user_can($bp->loggedin_user->id, 'view_user_distance')) {
            return false; //the user can not view the user's total
        }
    }
//echo '<p>' . $select . '</p>';
    $total_distance = $wpdb->get_var($select);
//var_dump($total_distance);
    if (!$total_distance) {
        $total_distance = 0;
    }

    return $total_distance;
}

function bp_runninglog_get_logs($user_id = null) {
    global $bp;
    global $wpdb;

    if (!$user_id) {
        $user = '';
    } else {
        $user = " `user_id` = $user_id ";
    }
    $where = '';
    if (!empty($user)) {
        $where .= $user;
    }
    $table = $bp->table_prefix . "bp_runninglog";
    $select = "SELECT `id`, `user_id`, `date_year`, `date_month`, `date_day`, `distance`, `time`, `event`, `event_cutoff`, `event_town` FROM `$table` " .
            (!empty($where) ? ' WHERE ' . $where : '') . " ORDER BY `date_year` DESC, `date_month` DESC, `date_day` DESC";

    if ($user_id != $bp->loggedin_user->id && $user_id) {//they do not match
        if (!user_can($bp->loggedin_user->id, 'delete_others_running_logs')) {
            return false; //the user can not delete the user's logs
        }
    }
    //echo '<p>' . $select . '</p>';
    $logs = $wpdb->get_results($select);

    return $logs;
}

function bp_runninglog_get_log($log_id = 0) {
    global $bp;
    global $wpdb;

    $table = $bp->table_prefix . "bp_runninglog";
    $select = "SELECT `id`, `user_id`, `date_year`, `date_month`, `date_day`, `distance`, `time`, `event`, `event_cutoff`, `event_town` FROM `$table` WHERE `id` = $log_id LIMIT 1";

    $log = $wpdb->get_results($select);

    return $log;
}

function bp_runninglog_get_target_achieved($user_id, $year = null) {
    global $bp;

    if ($user_id != $bp->loggedin_user->id) {//they do not match
        if (!user_can($bp->loggedin_user->id, 'view_user_distance')) {
            return false; //the user can not view the user's total
        }
    }

//first get the value of this user's set target
//$target = 1000; //get_user_target($userid);
    $target = (int) bp_get_profile_field_data('field=Target&user_id=' . $user_id); //get_user_meta($user_id, 'running_target', true);
//now determine the year
    if (!$year) {
        $year_now = date('Y');
        $month_now = date('n');
        if ($month_now > 5) {
            $year_1 = $year_now;
            $year_2 = $year_now + 1;
        } else {
            $year_1 = $year_now - 1;
            $year_2 = $year_now;
        }
        $year = $year_1 . '-' . $year_2;
    }

    $total_completed = bp_runninglog_get_total_distance($year, null, $user_id);
    if ($target == null || $target == 0) {
        $target = 1000;
    }
    if (!$total_completed) {
        $total_completed = 0;
    }
    $percentage2 = round($total_completed / $target * 100, 2);
    $percentage = $percentage2;
    if ($percentage2 > 100) { //the system does not allow for more than 100%
        $percentage = 100; //if you have reached more that 100%, you have reached your target
    }
    return array(
        'target' => $target,
        'completed' => $total_completed,
        'percentage' => $percentage,
        'percentage2' => $percentage2
    );
}

function bp_traininglog_get_logs($user_id = null) {
    global $bp;
    global $wpdb;

    if (!$user_id) {
        $user = '';
    } else {
        $user = " `user_id` = $user_id ";
    }
    $where = '';

    if (!empty($user)) {
        $where .= $user;
    }
    $table = $bp->table_prefix . "bp_traininglog";
    $select = "SELECT `id`, `date_year`, `date_month`, `date_day`, `distance`, `time` FROM `$table` " .
            (!empty($where) ? ' WHERE ' . $where : '') . " ORDER BY `date_year` DESC, `date_month` DESC, `date_day` DESC";

    //echo "check caps";
    if ($user_id != $bp->loggedin_user->id && $user_id) {//they do not match
        if (!user_can($bp->loggedin_user->id, 'delete_others_running_logs')) {
            return false; //the user can not delete the user's logs
        }
    }
    echo '<p>' . $select . '</p>';
    $logs = $wpdb->get_results($select);

    return $logs;
}

function bp_traininglog_get_log($log_id = 0) {
    global $bp;
    global $wpdb;

    $table = $bp->table_prefix . "bp_traininglog";
    $select = "SELECT `id`, `user_id`, `date_year`, `date_month`, `date_day`, `distance`, `time` FROM `$table` WHERE `id` = $log_id LIMIT 1";

    $log = $wpdb->get_results($select);

    return $log;
}

function bp_runninglog_get_stats($year = null, $user_id = null) {
    //global $bp;

    if (!$year) {
//now determine the year
        $year_now = date('Y');
        $month_now = date('n');
        if ($month_now > 5) {
            $year_1 = $year_now;
            $year_2 = $year_now + 1;
        } else {
            $year_1 = $year_now - 1;
            $year_2 = $year_now;
        }
        $year = $year_1 . '-' . $year_2;
    }
    if (strlen($year) > 4) {//now a single year is considdered the start year
        $years = explode('-', $year);
        $year_1 = $years[0];
        $year_2 = $years[1];
    } else {
        $year_1 = (int) $year;
        $year_2 = $year_1 + 1;
    }
    $comrades_date_1 = bp_runninglog_get_previous_comrades_dates($year_1);
    $comrades_date_2 = bp_runninglog_get_previous_comrades_dates($year_2);
    if (!$comrades_date_1) {
        //what is the default
        $comrades_date_1 = array(6, 1); //default date is 1 June
    }
    if (!$comrades_date_2) {
        $comrades_date_2 = $comrades_date_1; //use the previous year's date for exaclty 12 months by default
    }
    $offset = $comrades_date_1[0]; //this is the month of the comrades as start date
    $offset_day = $comrades_date_1[1];
    $limit = $comrades_date_2[0]; //the month of the next comdares as end date
    $limit_day = $comrades_date_2[1];
    $limit_day += 1; //add another day as less than is used here
    $offset_day += 1; //now the day after the comrades
    if ($offset_day == 32) {//a month can not have 32 days
        $offset = $offset + 1; //go to next month
        $offset_day = 0; //start with the first day - this is used with greater_than
    }

    //if ($offset_day == 31) {
    //$offset = $offset + 1; //go to next month
    //$offset_day = 1;
    //}
    /* if ($limit_day == 1) {
      $limit = $limit - 1; //if the next comrades is on the 1st of the month, use end of previous month
      //$limit_day = 32; //1 more than any months days to get all
      } */
    //now calculate the # of months between the two dates
    $months_to_get = 12;
    if ($offset == $limit) {
        $months_to_get = $months_to_get + 1; //the same month have to be received 2x
    }
    if ($offset < $limit) {
        $months_to_get = $months_to_get + 2; //there are 2 overlapping months
    }
    //if($offset > $limit){
    //this should be the default 12 months
    //}
    $stats = array();
    $max_distance = 1;
    $use_year = $year_1;
    /* $offset = 1;
      $use_year = $year;
      if ($running_year) {
      $offset = 6;
      }
      $stats = array();
      $max_distance = 1;
      for ($i = 0; $i < 12; $i++) { */
    for ($i = 0; $i < $months_to_get; $i++) {
        $month_number = $i + $offset;
        if ($month_number > 12) {
            $month_number -= 12;
            $use_year = $year_2;
        }
        $distance = bp_runninglog_get_total_distance($use_year, $month_number, $user_id, "$year_1-$year_2");
        if ($distance > $max_distance) {
            $max_distance = $distance;
        }
        $stats[($i + 1)] = array(
            'month_name' => date("M", mktime(0, 0, 0, $month_number, 1, 2011)),
            'month_number' => $month_number,
            'distance' => ($distance ? $distance : 0.00 )
        );
    }
    $stats_distance = array($stats, $max_distance);

    return $stats_distance;
}

function bp_traininglog_get_stats($year = null, $user_id = null) {

    $running_year = false;
    if (!$year) {
//now determine the year
        $year_now = date('Y');
        $month_now = date('n');
        if ($month_now > 5) {
            $year_1 = $year_now;
            $year_2 = $year_now + 1;
        } else {
            $year_1 = $year_now - 1;
            $year_2 = $year_now;
        }
        $year = $year_1 . '-' . $year_2;
        $running_year = true;
    }
    if (strlen($year) > 4) {
        $years = explode('-', $year);
        $year_1 = $years[0];
        $year_2 = $years[1];
        $running_year = true;
    }
    $offset = 1;
    $use_year = $year;
    if ($running_year) {
        $offset = 6;
        $use_year = $year_1;
    }
    $stats = array();
    $max_distance = 1;
    for ($i = 0; $i < 12; $i++) {
        $month_number = $i + $offset;
        if ($running_year && $month_number > 12) {
            $month_number -= 12;
            $use_year = $year_2;
        }
        $distance = bp_traininglog_get_total_distance($use_year, $month_number, $user_id);
        if ($distance > $max_distance) {
            $max_distance = $distance;
        }
        $stats[($i + 1)] = array(
            'month_name' => date("M", mktime(0, 0, 0, $month_number, 1, 2011)),
            'month_number' => $month_number,
            'distance' => ($distance ? $distance : 0 )
        );
    }
    $stats_distance = array($stats, $max_distance);

    return $stats_distance;
}

function bp_runninglog_running_stats_widget($atts, $content = null) {
    global $bp;
    $year = null;
    extract(shortcode_atts(array(
        "year" => null
                    ), $atts));
    $stats = bp_runninglog_get_stats($year);
    $year_now = date('Y');
    $month_now = date('n');
    if ($month_now > 5) {
        $year_1 = $year_now;
        $year_2 = $year_now + 1;
    } else {
        $year_1 = $year_now - 1;
        $year_2 = $year_now;
    }
    $year = $year_1 . '-' . $year_2;
    $output = '
        <p>This is the Running Log Total for all our users.</p>
<table id="runninglog-stats">
    <tr class="stat-bar-container">
        ';
    foreach ($stats[0] as $month_stats) {
        $percentage = round($month_stats['distance'] / $stats[1] * 100, 2);
        $output .= '
        <td>
            <div class="stat-bar" style="height:' . $percentage . '%"></div>
        </td>';
    }
    $output .= '</tr>
    <tr class="stats-months">';
    foreach ($stats[0] as $month_stats) {
        $output .= '<td><strong>' . $month_stats['month_name'] . ($month_now == $month_stats['month_number'] ? '*' : '') . '</strong></td>';
    }
    $output .= '</tr>
    <tr class="stats-distances">';
    foreach ($stats[0] as $month_stats) {
        $output .= '<td>' . $month_stats['distance'] . ' KM</td>';
    }
    $output .= '</tr>
</table>
';

    $output .= '<table id="runninglog-info">
    <tr>
        <th>Total Distance</th>
        <td>' . bp_runninglog_get_total_distance(null, null, null) . ' KM</td>
    </tr>
</table>';
    if ($bp->loggedin_user) {
        $output .= '<p>Update my own <a href="' . $bp->loggedin_user->domain . 'runninglog/">Running Log</a>.</p>';
    }

    return $output . $content;
}

add_shortcode("site_stats", "bp_runninglog_running_stats_widget");

function bp_directory_members_runninglog_item() {
    //global $bp;
    //echo "<pre>";
    //var_dump($bp);
    //echo "</pre>";
    $output = '<a class="btn" href="' . bp_core_get_user_domain(bp_get_member_user_id()) . 'runninglog/"><i class="icon-calendar"></i> Running Log</a>';
    $output .= '<a class="btn" href="' . bp_core_get_user_domain(bp_get_member_user_id()) . 'runninglog/awards/"><i class="icon-certificate"></i> Awards</a>';
    echo '<br /><div class="btn-group">' . $output . '</div>';
}

add_action('bp_directory_members_item', 'bp_directory_members_runninglog_item');



/* ADMIN FUNCTIONS */

function bp_running_log_get_report_data($report_fiters, $report_settings) {
    $all_user_ids =
            $gender_userids =
            $runnertype_userids =
            $region_userids =
            $agegroups_userids =
            bp_runninglog_get_all_userids();

    if ($report_fiters['gender'] != 'all') {
        //we need to get the gender information
        $gender_userids = bp_runninglog_get_userids_from_gender($report_settings['gender'][$report_fiters['gender']]);
    }
    if ($report_fiters['runnertype'] != 'all') {
        //we need to get the runnertype information
        $runnertype_userids = bp_runninglog_get_userids_from_runnertype($report_settings['runnertype'][$report_fiters['runnertype']]);
    }
    if ($report_fiters['region'] != 'all') {
        //we need to get the province information
        $region_userids = bp_runninglog_get_userids_from_region($report_settings['region'][$report_fiters['region']]);
    }
    if ($report_fiters['agegroups'] != 'all') {
        //we need to get the agegroups information
        $agegroups_userids = bp_runninglog_get_userids_from_agegroup($report_settings['agegroups'][$report_fiters['agegroups']]);
    }
    //if ($report_fiters['time'] != 'all') {
    //this is not profile information thus not needed now
    //}
    //the final array containing all userids that is in all arrays below
    $user_ids = array_intersect($all_user_ids, $gender_userids, $runnertype_userids, $region_userids, $agegroups_userids);
    //$user_ids = $all_user_ids;
    if (empty($user_ids)) {
        return false; //we have nothing
    }
    $year = (!empty($report_fiters['year']) ? $report_fiters['year'] : null);
    if ($year == 'all') {
        $year = null;
    }
    $users = array();
    foreach ($user_ids as $user_id) {
        $users[] = array(
            'user_id' => $user_id,
            'distance' => bp_runninglog_get_total_distance($year, null, $user_id, null, true),
            'total_distance' => bp_runninglog_get_total_distance(null, null, $user_id, null, true)
        );
    }
    //now sort the array
    if ($report_fiters['sort'] == 'desc') {
        usort($users, 'bp_runninglog_sort_desc'); //this sorts
    } else {
        usort($users, 'bp_runninglog_sort_asc'); //this sorts
    }
    return $users;
}

function bp_runninglog_sort_asc($a, $b) {
    if ($a['distance'] == $b['distance']) {
        return 0;
    }
    return ($a['distance'] < $b['distance']) ? -1 : 1;
}

function bp_runninglog_sort_desc($a, $b) {
    if ($a['distance'] == $b['distance']) {
        return 0;
    }
    return ($a['distance'] > $b['distance']) ? -1 : 1;
}

function bp_runninglog_get_userids_from_gender($gender) {
    global $wpdb, $bp;
    $xprofile_field = 3; //this is the database ID of the field which information we need
    $db = $wpdb->get_results($wpdb->prepare("
SELECT `user_id` FROM {$bp->table_prefix}bp_xprofile_data
WHERE `field_id` = $xprofile_field && `value` = '%1s'
", $gender));
    $return = array();
    foreach ($db as $info) {
        $return[] = $info->user_id;
    }
    return $return; //we need an array
}

function bp_runninglog_get_all_userids() {
    global $wpdb, $bp;

    $db = $wpdb->get_results("
SELECT `ID` FROM {$bp->table_prefix}users");
    $return = array();
    foreach ($db as $info) {
        $return[] = (int) $info->ID;
    }
    sort($return);
    return $return; //we need an array
}

function bp_runninglog_get_userids_from_runnertype($runnertype) {
    global $wpdb, $bp;

    $xprofile_field = 14; //this is the database ID of the field which information we need
    $query = $wpdb->prepare("
SELECT `user_id` FROM {$bp->table_prefix}bp_xprofile_data
WHERE `field_id` = $xprofile_field && `value` = '%1s'
", $runnertype);
    //echo "<p>$query</p>";
    $db = $wpdb->get_results($query);
    $return = array();
    foreach ($db as $info) {
        $return[] = $info->user_id;
    }
    return $return; //we need an array
}

function bp_runninglog_get_userids_from_region($region) {
    global $wpdb, $bp;

    $xprofile_field = 21; //this is the database ID of the field which information we need
    $db = $wpdb->get_results($wpdb->prepare("
SELECT `user_id` FROM {$bp->table_prefix}bp_xprofile_data
WHERE `field_id` = $xprofile_field && `value` = '%1s'
", $region));
    $return = array();
    foreach ($db as $info) {
        $return[] = $info->user_id;
    }
    return $return; //we need an array
}

function bp_runninglog_get_userids_from_agegroup($agegroup) {
    global $wpdb, $bp;

    $year = date("Y");

    $agegroups = array(
        'Under 40' => 40,
        '40 to 49' => 50,
        '50 to 59' => 60,
        '60 to 69' => 70,
        '70 to 79' => 80,
        '80+' => 120, //an age that nobody would be over
    );
    $get_group = $agegroups[$agegroup];

    $xprofile_field = 2; //this is the database ID of the field which information we need
    $db = $wpdb->get_results("
SELECT `user_id`,`value` FROM {$bp->table_prefix}bp_xprofile_data
WHERE `field_id` = $xprofile_field
");
    //these arrays will ontain the different
    $user_ids = array(
        40 => array(),
        50 => array(),
        60 => array(),
        70 => array(),
        80 => array(),
        120 => array(),
    );
    foreach ($db as $user_info) {
        $dobtime = strtotime(str_replace(' 00:00:00', '', $user_info->value));
        $age = $year - date("Y", $dobtime);
        $bday = date("m/d/", $dobtime) . $year;
        if ((time() - strtotime($bday)) < 0)
            $age = $age - 1;
        //now populate the arrays, a user_id can only be in 1 array
        if ($age < 40) {
            $user_ids[40][] = $user_info->user_id;
        } elseif ($age < 50) {
            $user_ids[50][] = $user_info->user_id;
        } elseif ($age < 60) {
            $user_ids[60][] = $user_info->user_id;
        } elseif ($age < 70) {
            $user_ids[70][] = $user_info->user_id;
        } elseif ($age < 80) {
            $user_ids[80][] = $user_info->user_id;
        } elseif ($age < 120) {
            $user_ids[120][] = $user_info->user_id;
        }
    }
    return $user_ids[$get_group];
}

function bp_runninglog_get_report_name($report, $report_settings/* , $times */) {
    /* $str_nrs = array(
      1 => 'runnertype',
      2 => 'gender',
      3 => 'region',
      4 => 'agegroup',
      5 => 'year',
      ); */
    $report_names = array(
        'default' => 'Top Overall, All Time', //the default
        'runnertype' => 'Top %1$s Overall, All Time', //only runningtype set
        "runnertype_gender" => 'Top %2$s %1$s, All Time',
        "runnertype_gender_region" => 'Top %2$s %1$s from %3$s, All Time',
        "runnertype_gender_region_agegroup" => 'Top %4$s %2$s %1$s from %3$s, All Time',
        "runnertype_gender_region_agegroup_year" => 'Top %4$s %2$s %1$s from %3$s, for %5$s',
        "runnertype_gender_agegroup" => 'Top %4$s %2$s %1$s, All Time',
        "runnertype_gender_agegroup_year" => 'Top %4$s %2$s %1$s, for %5$s',
        "runnertype_gender_year" => 'Top %2$s %1$s, for %5$s',
        "runnertype_region" => 'Top %1$s from %3$s, All Time',
        "runnertype_region_agegroup" => 'Top %4$s %1$s from %3$s, All Time',
        "runnertype_region_agegroup_year" => 'Top %4$s %1$s from %3$s, for %5$s',
        "runnertype_region_year" => 'Top %1$s from %3$s, for %5$s',
        "runnertype_agegroup" => 'Top %4$s %1$s, All Time',
        "runnertype_agegroup_year" => 'Top %4$s %1$s, for %5$s',
        "runnertype_year" => 'Top %1$s, for %5$s',
        "gender" => 'Top %2$s Overall, All Time', //only gender set
        "gender_region" => 'Top %2$s, from %3$s, All Time',
        "gender_region_agegroup" => 'Top %4$s %2$s, from %3$s, All Time',
        "gender_region_agegroup_year" => 'Top %4$s %2$s, from %3$s, for %5$s',
        "gender_region_year" => 'Top %2$s, from %3$s, for %5$s',
        "gender_agegroup" => 'Top %4$s %2$s, All Time',
        "gender_agegroup_year" => 'Top %4$s %2$s, for %5$s',
        "gender_year" => 'Top %2$s for %5$s',
        'region' => 'Top %3$s Overall, All Time', //province set
        "region_agegroup" => 'Top %3$s %4$s, All Time',
        "region_agegroup_year" => 'Top %3$s %4$s, for %5$s',
        "region_year" => 'Top %3$s for %5$s',
        "agegroup" => 'Top %4$s Overall, All Time', //agegroup set
        "agegroup_year" => 'Top %4$s for %5$s',
        "year" => 'Top for %5$s', //if only date is set
    );
    $string = array();
    if ($report['runnertype'] != 'all') {
        $string[] = 'runnertype';
    }
    if ($report['gender'] != 'all') {
        $string[] = 'gender';
    }
    if ($report['region'] != 'all') {
        $string[] = 'region';
    }
    if ($report['agegroups'] != 'all') {
        $string[] = 'agegroup';
    }
    if ($report['year'] != 'all') {
        $string[] = 'year';
    }
    //$components = $string;
    $string2 = implode('_', $string);
    //var_dump($components);
    //var_dump($string);
    //var_dump($report);

    $runnertype = $report_settings['runnertype'][$report['runnertype']] . 's';
    $gender = $report_settings['gender'][$report['gender']];
    if ($gender == 'Male') {
        $gender = 'Men';
    }
    if ($gender == 'Female') {
        $gender = 'Woman';
    }
    $region = $report_settings['region'][$report['region']];
    $agegroup = $report_settings['agegroups'][$report['agegroups']];

    $return = (array_key_exists($string2, $report_names) ? $report_names[$string2] : $report_names['default']);
    //var_dump($return);
    return sprintf($return, $runnertype, $gender, $region, $agegroup, $report['year']);
}

function bp_runninglog_get_previous_comrades_dates($year) {
    $all_dates = bp_runninglog_get_all_comrades_dates();
    if (array_key_exists($year, $all_dates))
        return $all_dates[$year];
    else
        return false;
}

function bp_runninglog_get_all_comrades_dates() {
    global $comrades_dates;
    $saved_file = dirname(__FILE__) . '/comrades.txt';
    if ($comrades_dates && !empty($comrades_dates)) {
        return $comrades_dates; //do not read file if we already have it - save time
    }
    if (file_exists($saved_file)) {
        $content = file_get_contents($saved_file);
        if (!empty($content)) {
            $comrades_dates = unserialize($content);
            return $comrades_dates;
        }
    }
    return false;
}

function bp_runninglog_save_comrades_date($year, $month, $day) {
    global $comrades_dates;
    $saved_file = dirname(__FILE__) . '/comrades.txt';
    if (file_exists($saved_file)) {
        $content = file_get_contents($saved_file);
        if (!empty($content)) {
            $current_content = unserialize($content);
        } else {
            $current_content = array();
        }
    } else {
        $current_content = array();
    }
    $current_content[$year] = array($month, $day); //insert it into the array
    $comrades_dates = $current_content; //save it to the global to use it everywhere
    return file_put_contents($saved_file, serialize($current_content));
}

function bp_runninglog_save_csv($user_id, $csv_file = '') {
    $data = bp_runninglog_csv_to_array($csv_file);
    foreach ($data as $value) {
        $event = $value['Event Name'];
        $event_cutoff = $value['Event Cutoff Time'];
        $event_town = $value['Event Town'];
        $distance = $value['Distance'];
        $time = $value['Time'];
        //$ch_year = $value['Challenge Year'];
        //if (strlen($ch_year) > 4) {
        //$ch_year = (int) (explode('-', $ch_year)[0]); //allways use the first year as an integer
        //}
        $date = array(
            'day' => $value['Day'],
            'month' => $value['Month'],
            'year' => $value['Year'],
                //'ch_year' => $ch_year,
        );
        bp_runninglog_save_log($user_id, $event, $event_cutoff, $event_town, $date, $distance, $time, 0, true);
    }
}

function bp_runninglog_csv_to_array($filename = '', $delimiter = ',') {
    if (!file_exists($filename) || !is_readable($filename))
        return FALSE;

    $header = NULL;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== FALSE) {
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
            if (!$header){
                $header = $row;
            }else{
				$header_count = count($header);
				$row_count = count($row);
				while($row_count < $header_count){//only enter if it is true
					$row[] = '';//add an empty element
					$row_count++;//increment
				}
                $data[] = array_combine($header, $row);
			}
        }
        fclose($handle);
    }
    return $data;
}

/*
 * Awards
 *
 * This part is for the Awards Section of the Running Log
 */

function range_possible_end($current, $updown, $stop) {
    if ($updown == 'up') {
        return ($current > $stop) ? false : true;
    }
    if ($updown == 'down') {
        return ($current < $stop) ? false : true;
    }
}

function range_iteration($current, $j = 1) {
    return $current + $j;
}

/**
 * Deletes an Activity item received via a POST request.
 *
 * @return mixed String on error, void on success
 * @since BuddyPress (1.2)
 */
function bp_runninglog_ajax_delete_log() {
    // Bail if not a POST action
    if ('POST' !== strtoupper($_SERVER['REQUEST_METHOD']))
        return;

    if (!is_user_logged_in())
        exit('Not Logged In');

    if (empty($_POST['id']))
        exit('Post Id' . $_POST['id']);

    $ids = explode('-', $_POST['id']);
    $log_id = (int) $ids[0];
    $user_id = (int) $ids[1];

    // Check the nonce
    check_admin_referer('bp_runninglog_deletelog_' . $log_id);

    if (bp_runninglog_delete_log($user_id, $log_id)) {
        exit;
//exit('Successful');
    }
    exit('Failure');

    //$activity = new BP_Activity_Activity( (int) $_POST['id'] );
    // Check access
    //if ( ! bp_activity_user_can_delete( $activity ) )
    //exit( '-1' );
    // Call the action before the delete so plugins can still fetch information about it
    //do_action( 'bp_activity_before_action_delete_activity', $activity->id, $activity->user_id );
    //if ( ! bp_activity_delete( array( 'id' => $activity->id, 'user_id' => $activity->user_id ) ) )
    //exit( '-1<div id="message" class="error"><p>' . __( 'There was a problem when deleting. Please try again.', 'buddypress' ) . '</p></div>' );
    //do_action( 'bp_activity_action_delete_activity', $activity->id, $activity->user_id );
    exit;
}

add_action('wp_ajax_delete_runninglog', 'bp_runninglog_ajax_delete_log');


function generate_full_report_for_year($year){
    global $wpdb;
    if('GET' !== strtoupper($_SERVER['REQUEST_METHOD'])){
        return;
    }
    if(!current_user_can('running_reports')){
        return;
    }

    $year_1 = (int) $year;
    $year_2 = $year_1 + 1;
    $comrades_date_1 = bp_runninglog_get_previous_comrades_dates($year_1);
    $comrades_date_2 = bp_runninglog_get_previous_comrades_dates($year_2);
    if (!$comrades_date_1) {
        $comrades_date_1 = array(6, 1); //default date is 1 June
    }
    if (!$comrades_date_2) {
        $comrades_date_2 = $comrades_date_1; //use the previous year's date for exactly 12 months by default
    }
    $offset = $comrades_date_1[0]; //this is the month of the comrades as start date
    $offset_day = $comrades_date_1[1]; //this is the day of comrades
    $limit = $comrades_date_2[0]; //the month of the next comdares as end date
    $limit_day = $comrades_date_2[1]; //this is the day before the next comrades
    $limit_day += 1; //add another day as less than is used here
    if ($offset_day == 32) {//a month can not have 32 days
        $offset = $offset + 1; //go to next month
        $offset_day = 0; //start with the first day - this is used with greater_than
    }
    $query = "
    SELECT 
        `user_id`
      , `date_year`
      , `date_month`
      , `date_day`
      , `distance`
      , `event`
      , `time` 
    FROM `1000kmch_bp_runninglog` 
    WHERE (
        (`date_year` = '$year_1' && `date_month` > '$offset')
          OR
        (`date_year` = '$year_2' && `date_month` < '$limit')
          OR
        (`date_year` = '$year_1' && `date_month` = '$offset' && `date_day` > '$offset_day')
          OR
        (`date_year` = '$year_2' && `date_month` = '$limit' && `date_day` < '$limit_day')
    ) 
    ORDER BY `user_id` ASC, `date_year` ASC, `date_month` ASC, `date_day` ASC";

    return $wpdb->get_results($query, ARRAY_A);
}

#EOL