<?php

// Exit if accessed directly
// It's a good idea to include this in each of your plugin files, for increased security on
// improperly configured servers
if (!defined('ABSPATH'))
    exit;

if (file_exists(dirname(__FILE__) . '/languages/' . get_locale() . '.mo'))
    load_textdomain('bp-runninglog', dirname(__FILE__) . '/languages/' . get_locale() . '.mo');

/**
 * Implementation of BP_Component
 *
 * BP_Component is the base class that all BuddyPress components use to set up their basic
 * structure, including global data, navigation elements, and admin bar information. If there's
 * a particular aspect of this class that is not relevant to your plugin, just leave it out.
 *
 * @package BuddyPress_Skeleton_Component
 * @since 1.6
 */
class BP_Runninglog_Component extends BP_Component {

    /**
     * Constructor method
     *
     * You can do all sorts of stuff in your constructor, but it's recommended that, at the
     * very least, you call the parent::start() function. This tells the parent BP_Component
     * to begin its setup routine.
     *
     * @package BuddyPress_Skeleton_Component
     * @since 1.6
     */
    function __construct() {
        global $bp;

        parent::start('runninglog', __('Running Log', 'bp-runninglog'), BP_RUNNINGLOG_PLUGIN_DIR);

        /**
         * BuddyPress-dependent plugins are loaded too late to depend on BP_Component's
         * hooks, so we must call the function directly.
         */
        $this->includes();

        $bp->active_components[$this->id] = '1';

        add_action('init', array(&$this, 'register_post_types'));
    }

    /**
     * Include your component's files
     *
     * @package BuddyPress_Skeleton_Component
     * @since 1.6
     */
    function includes($includes = array()) {
        // Files to include
        $includes += array(
            'includes/bp-runninglog-actions.php',
            'includes/bp-runninglog-screens.php',
            //'includes/bp-runninglog-filters.php',
            //'includes/bp-runninglog-classes.php',
            //'includes/bp-runninglog-activity.php',
            'includes/bp-runninglog-template.php',
            'includes/bp-runninglog-functions.php',
            'includes/bp-runninglog-notifications.php',
            //'includes/bp-runninglog-widgets.php',
            'includes/bp-runninglog-cssjs.php',
                //'includes/bp-runninglog-ajax.php'
        );

        parent::includes($includes);

        // As an example of how you might do it manually, let's include the functions used
        // on the WordPress Dashboard conditionally:
        if (is_admin() || is_network_admin()) {
            include( BP_RUNNINGLOG_PLUGIN_DIR . '/includes/bp-runninglog-admin.php' );
        }
    }

    /**
     * Set up your plugin's globals
     *
     * Use the parent::setup_globals() method to set up the key global data for your plugin:
     *   - 'slug'			- This is the string used to create URLs when your component
     * 				  adds navigation underneath profile URLs. For example,
     * 				  in the URL http://testbp.com/members/boone/example, the
     * 				  'example' portion of the URL is formed by the 'slug'.
     * 				  Site admins can customize this value by defining
     * 				  BP_EXAMPLE_SLUG in their wp-config.php or bp-custom.php
     * 				  files.
     *   - 'root_slug'		- This is the string used to create URLs when your component
     * 				  adds navigation to the root of the site. In other words,
     * 				  you only need to define root_slug if your component is a
     * 				  "root component". Eg, in:
     * 				    http://testbp.com/example/test
     * 				  'example' is a root slug. This should always be defined
     * 				  in terms of $bp->pages; see the example below. Site admins
     * 				  can customize this value by changing the permalink of the
     * 				  corresponding WP page in the Dashboard. NOTE:
     * 				  'root_slug' requires that 'has_directory' is true.
     *   - 'has_directory'		- Set this to true if your component requires a top-level
     * 				  directory, such as http://testbp.com/example. When
     * 				  'has_directory' is true, BP will require that site admins
     * 				  associate a WordPress page with your component. NOTE:
     * 				  When 'has_directory' is true, you must also define your
     * 				  component's 'root_slug'; see previous item. Defaults to
     * 				  false.
     *   - 'notification_callback'  - The name of the function that is used to format BP
     * 				  admin bar notifications for your component.
     *   - 'search_string'		- If your component is a root component (has_directory),
     * 				  you can provide a custom string that will be used as the
     * 				  default text in the directory search box.
     *   - 'global_tables'		- If your component creates custom database tables, store
     * 				  the names of the tables in a $global_tables array, so that
     * 				  they are available to other BP functions.
     *
     * You can also use this function to put data directly into the $bp global.
     *
     * @package BuddyPress_Skeleton_Component
     * @since 1.6
     *
     * @global obj $bp BuddyPress's global object
     */
    function setup_globals($args = array()) {
        global $bp;

        // Defining the slug in this way makes it possible for site admins to override it
        if (!defined('BP_RUNNINGLOG_SLUG'))
            define('BP_RUNNINGLOG_SLUG', $this->id);

        // Global tables for the example component. Build your table names using
        // $bp->table_prefix (instead of hardcoding 'wp_') to ensure that your component
        // works with $wpdb, multisite, and custom table prefixes.
        $global_tables = array(
            'runninglog' => $bp->table_prefix . 'bp_runninglog',
            'award_rels' => $bp->table_prefix . 'bp_award_rels'
        );

        // Set up the $globals array to be passed along to parent::setup_globals()
        $globals = array(
            'slug' => BP_RUNNINGLOG_SLUG,
            'root_slug' => isset($bp->pages->{$this->id}->slug) ? $bp->pages->{$this->id}->slug : BP_RUNNINGLOG_SLUG,
            'has_directory' => true, // Set to false if not required
            //'notification_callback' => 'bp_runninglog_format_notifications',
            //'search_string' => __('Search Runnning Logs...', 'buddypress'),
            'global_tables' => $global_tables
        );

        // Let BP_Component::setup_globals() do its work.
        parent::setup_globals($globals);

        // If your component requires any other data in the $bp global, put it there now.
        //$bp->{$this->id}->misc_data = '123';
    }

    /**
     * Set up your component's navigation.
     *
     * The navigation elements created here are responsible for the main site navigation (eg
     * Profile > Activity > Mentions), as well as the navigation in the BuddyBar. WP Admin Bar
     * navigation is broken out into a separate method; see
     * BP_Example_Component::setup_admin_bar().
     *
     * @global obj $bp
     */
    function setup_nav($main_nav = array(),$sub_nav = array()) {
        global $bp;
        // Add 'Example' to the main navigation
        $main_nav = array(
            'name' => __('Running Log', 'bp-runninglog'),
            'slug' => 'runninglog',
            'position' => 25,
            'screen_function' => 'bp_runninglog_stats',
            'default_subnav_slug' => 'stats'
        );

        $user_domain = bp_displayed_user_domain() ? bp_displayed_user_domain() : bp_loggedin_user_domain();
        $profile_link = trailingslashit($user_domain . 'runninglog');
        //$runninglog_link = trailingslashit(bp_loggedin_user_domain() . 'runninglog');
        // Add a few subnav items under the main Example tab
        $sub_nav[] = array(
            'name' => __('Running Stats', 'bp-runninglog'),
            'slug' => 'stats',
            'parent_url' => $profile_link,
            'parent_slug' => 'runninglog',
            'screen_function' => 'bp_runninglog_stats',
            'position' => 10
        );
        $sub_nav[] = array(
            'name' => __('Training Stats', 'bp-runninglog'),
            'slug' => 'training',
            'parent_url' => $profile_link,
            'parent_slug' => 'runninglog',
            'screen_function' => 'bp_traininglog_stats',
            'position' => 25
        );

        // Add the subnav items to the friends nav item
        $sub_nav[] = array(
            'name' => __('Running Awards', 'bp-runninglog'),
            'slug' => 'awards',
            'parent_url' => $profile_link,
            'parent_slug' => 'runninglog',
            'screen_function' => 'bp_runninglog_awards',
            'position' => 20,
            'user_has_access' => true
        );

        // Add the subnav items to the friends nav item
        if (bp_is_runninglog_component() && ($bp->loggedin_user->id == $bp->displayed_user->id || user_can($bp->loggedin_user->id, 'delete_others_running_logs'))) {
            $sub_nav[] = array(
                'name' => __('Edit Running Logs', 'bp-runninglog'),
                'slug' => 'logs',
                'parent_url' => $profile_link,
                'parent_slug' => 'runninglog',
                'screen_function' => 'bp_runninglog_logs',
                'position' => 15,
                'user_has_access' => true
            );
            $sub_nav[] = array(
                'name' => __('Edit Training Logs', 'bp-runninglog'),
                'slug' => 'traininglogs',
                'parent_url' => $profile_link,
                'parent_slug' => 'runninglog',
                'screen_function' => 'bp_traininglog_logs',
                'position' => 30,
                'user_has_access' => true
            );
        }

        parent::setup_nav($main_nav, $sub_nav);

        /* bp_core_new_nav_item(
          array(
          'name' => 'Running Log',
          'slug' => 'runninglog',
          'show_for_displayed_user' => false,
          'position' => 70,
          'default_subnav_slug' => 'stats',
          'screen_function' => 'bp_runninglog_stats'
          )
          ); */


        // If your component needs additional navigation menus that are not handled by
        // BP_Component::setup_nav(), you can register them manually here. For example,
        // if your component needs a subsection under a user's Settings menu, add
        // it like this. See bp_example_screen_settings_menu() for more info
        /* bp_core_new_subnav_item(array(
          'name' => __('Running Log', 'bp-runninglog'),
          'slug' => 'runninglog-admin',
          'parent_slug' => bp_get_settings_slug(),
          'parent_url' => trailingslashit(bp_loggedin_user_domain() . bp_get_settings_slug()),
          'screen_function' => 'bp_runninglog_screen_settings_menu',
          'position' => 40,
          'user_has_access' => bp_is_my_profile() // Only the logged in user can access this on his/her profile
          )); */
    }

    /**
     * If your component needs to store data, it is highly recommended that you use WordPress
     * custom post types for that data, instead of creating custom database tables.
     *
     * In the future, BuddyPress will have its own bp_register_post_types hook. For the moment,
     * hook to init. See BP_Example_Component::__construct().
     *
     * @package BuddyPress_Skeleton_Component
     * @since 1.6
     * @see http://codex.wordpress.org/Function_Reference/register_post_type
     */
    function register_post_types() {

        register_post_type(
                'awards', array(
            'label' => __('Awards', 'bp-runninglog'),
            'labels' => array(
                'name' => __('Awards', 'bp-runninglog'),
                'singular_name' => __('Award', 'bp-runninglog'),
                'menu_name' => __('Awards', 'bp-runninglog'),
                'add_new_item' => __('Create New Award', 'bp-runninglog'),
                'edit_item' => __('Edit Award', 'bp-runninglog'),
                'new_item' => __('New Award', 'bp-runninglog'),
                'view_item' => __('View Awards', 'bp-runninglog'),
                'search_item' => __('Search Awards', 'bp-runninglog'),
                'not_found' => __('No Awards Found', 'bp-runninglog'),
                'not_found_in_trash' => __('No Awards Found in the Trash', 'bp-runninglog')
            ),
            'description' => 'A Post Type of available Awards that can be awarded to users.',
            'public' => false,
            'exclude_from_search' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_nav_menus' => false,
            'show_in_menu' => true,
            'show_in_admin_bar' => false,
            'menu_position' => 100,
            'menu_icon' => null,
            'capability_type' => 'award',
            'map_meta_cap' => true,
            'hierarchical' => true,
            'supports' => array(
                'title',
                'editor',
                'thumbnail',
                'page-attributes',
            ),
            'rewrite' => false
                )
        );

        parent::register_post_types();
    }

    function register_taxonomies() {

    }

}

/**
 * Loads your component into the $bp global
 *
 * This function loads your component into the $bp global. By hooking to bp_loaded, we ensure that
 * BP_Example_Component is loaded after BuddyPress's core components. This is a good thing because
 * it gives us access to those components' functions and data, should our component interact with
 * them.
 *
 * Keep in mind that, when this function is launched, your component has only started its setup
 * routine. Using print_r( $bp->example ) or var_dump( $bp->example ) at the end of this function
 * will therefore only give you a partial picture of your component. If you need to dump the content
 * of your component for troubleshooting, try doing it at bp_init, ie
 *   function bp_example_var_dump() {
 *   	  global $bp;
 * 	  var_dump( $bp->example );
 *   }
 *   add_action( 'bp_init', 'bp_example_var_dump' );
 * It goes without saying that you should not do this on a production site!
 *
 * @package BuddyPress_Skeleton_Component
 * @since 1.6
 */
function bp_runninglog_load_core_component() {
    global $bp;

    $bp->example = new BP_Runninglog_Component;
}

add_action('bp_loaded', 'bp_runninglog_load_core_component');

/**
 * Return the component's slug
 *
 * Having a template function for this purpose is not absolutely necessary, but it helps to
 * avoid too-frequent direct calls to the $bp global.
 *
 * @package BuddyPress_Skeleton_Component
 * @since 1.6
 *
 * @uses apply_filters() Filter 'bp_get_example_slug' to change the output
 * @return str $example_slug The slug from $bp->example->slug, if it exists
 */
function bp_get_runninglog_slug() {
    global $bp;

    // Avoid PHP warnings, in case the value is not set for some reason
    $runninglog_slug = isset($bp->runninglog->slug) ? $bp->runninglog->slug : '';

    return apply_filters('bp_get_runninglog_slug', $runninglog_slug);
}

#EOF