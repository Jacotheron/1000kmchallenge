<?php

function bp_is_runninglog_component() {
    $is_runninglog_component = bp_is_current_component('runninglog');

    return apply_filters('bp_is_runninglog_component', $is_runninglog_component);
}

function bp_runninglog_slug() {
    echo bp_get_runninglog_slug();
}

function bp_runninglog_root_slug() {
    echo bp_get_runninglog_root_slug();
}

function bp_get_runninglog_root_slug() {
    global $bp;

    // Avoid PHP warnings, in case the value is not set for some reason
    $runninglog_root_slug = isset($bp->runninglog->root_slug) ? $bp->runninglog->root_slug : '';

    return apply_filters('bp_get_runninglog_root_slug', $runninglog_root_slug);
}

#EOF