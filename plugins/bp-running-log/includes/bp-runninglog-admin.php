<?php
/* * *
 * This file is used to add site administration menus to the WordPress backend.
 *
 * If you need to provide configuration options for your component that can only
 * be modified by a site administrator, this is the best place to do it.
 *
 * However, if your component has settings that need to be configured on a user
 * by user basis - it's best to hook into the front end "Settings" menu.
 */

function bp_runninglog_add_admin_menu() {
    //global $bp;
    $page_title = 'Running Log';
    $menu_title = 'Running Reports';
    $capability = 'running_reports';
    $menu_slug = 'runninglog';
    $function = 'pb_runninglog_reports_index';
    $icon_url = '';
    $position = 150;
    add_menu_page($page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position);
    add_submenu_page($menu_slug, 'Comrades Dates', 'Comrades Dates', $capability, $menu_slug . '_comrades', 'pb_runninglog_comrades_dates');
}

// The bp_core_admin_hook() function returns the correct hook (admin_menu or network_admin_menu),
// depending on how WordPress and BuddyPress are configured
add_action('admin_menu', 'bp_runninglog_add_admin_menu');

function pb_runninglog_reports_index() {
    $this_year = date('Y');
    $this_month = date('n');
    if ($this_month < 6) {
        $default_year = $this_year - 1; //this year started last year
    } else {
        $default_year = $this_year;
    }
    $report = array(
        'gender' => (isset($_GET['gender']) ? $_GET['gender'] : 'all'),
        'runnertype' => (isset($_GET['runnertype']) ? $_GET['runnertype'] : 'all'),
        'region' => (isset($_GET['region']) ? $_GET['region'] : 'all'),
        'agegroups' => (isset($_GET['agegroups']) ? $_GET['agegroups'] : 'all'),
        'sort' => (isset($_GET['sort']) ? $_GET['sort'] : 'desc'),
        /* 'time' => (isset($_GET['time']) ? $_GET['time'] : 'all'), */
        'year' => (isset($_GET['year']) ? $_GET['year'] : (string) $default_year), //default to current running year
        'type' => (isset($_GET['type']) ? $_GET['type'] : 'std') //default to current running year
    );

    $report_settings = array(
        'gender' => array(
            'all' => 'All Genders',
            'male' => 'Male',
            'female' => 'Female',
        ),
        'runnertype' => array(
            'all' => 'All Runner Types',
            'runner' => 'Runner',
            'walker' => 'Walker',
            'wheelchair' => 'Wheelchair',
        ),
        'region' => array(
            'all' => 'All Regions',
            '1' => 'Central Gauteng &amp; Vaal Triangle',
            '2' => 'Gauteng North',
            '3' => 'Boland, Border, Eastern Province, South Western Districts, Western Province',
            '4' => 'Mpumulanga, KwaZulu Natal, Limpopo, Central North West, Free State, Griqualand West'
        ),
        'agegroups' => array(
            'all' => 'All Age Groups',
            'under40' => 'Under 40',
            '40-49' => '40 to 49',
            '50-59' => '50 to 59',
            '60-69' => '60 to 69',
            '70-79' => '70 to 79',
            '80plus' => '80+',
        ),
        'time' => array(),
        'sort' => array(
            'desc' => 'Descending Order',
            'asc' => 'Ascending Order'
        )
    );
    $time_optgroup_ranges = array();
    for ($i = $this_year; $i > 1989; $i--) {
        $time_optgroup_ranges[] = "$i"; //add one year more
    }
    $report_name = bp_runninglog_get_report_name($report, $report_settings);
    ?>
    <div class="wrap">
        <h2><?php _e('Running Log Reports', 'bp-runninglog') ?></h2>
        <div class="report_filters">
            <form action="" method="get">
                <input type="hidden" value="runninglog" name="page" />
                <?php
                foreach ($report_settings as $setting_name => $values) {
                    //render the label
                    if ($setting_name == 'time') {
                        echo " <select name='year' id='year'>";
                        echo "<option value='all'" . ($report['year'] == 'all' ? ' selected="selected"' : '') . ">All Years</option>";
                        foreach ($time_optgroup_ranges as $range) {
                            echo "<option value='$range'" . ($report['year'] === (string) $range ? ' selected="selected"' : '') . ">$range</option>";
                        }
                        echo "</select> \n";
                    } else {
                        echo "<select id='$setting_name' name='$setting_name' style='max-width:200px;'>";
                        foreach ($values as $k => $v) {
                            echo "<option value='$k'" . ($report[$setting_name] == $k ? ' selected="selected"' : '') . ">$v</option>";
                        }
                        echo "</select> \n";
                    }
                    echo " &nbsp;&nbsp;|&nbsp;&nbsp; ";
                }
                ?>
                <input type="submit" class="button button-primary button-large" name="Update Report" value="Update Report" /><br />
                Format: <select name="format">
                    <option value="csv">CSV (Excel)</option>
                    <option value="html">Table</option>
                </select>&nbsp;&nbsp;|&nbsp;&nbsp;
                Export Limit: <input type="text" name="limit" value="<?php $explimit = count_users(); echo $explimit["total_users"]; ?>" placeholder="Export Row Limit" />&nbsp;&nbsp;|&nbsp;&nbsp;
                <input type="submit" class="button" name="export_running_log" value="Export Report" />
                <br/>
                Export a full report for a specific year. This will include all runners' races as entries.
                <select name='full_log_year' id='full_log_year'>
                    <?php
                    foreach ($time_optgroup_ranges as $range) {
                        echo "<option value='$range'" . ($report['year'] === (string) $range ? ' selected="selected"' : '') . ">$range</option>";
                    } ?>
                </select>&nbsp;&nbsp;|&nbsp;&nbsp;
                <input type="submit" class="button" name="export_full_running_log" id="export_full_running_log" value="Export Full Running Log" />
            </form>
            <p><a href="admin.php?page=runninglog">Clear Filters</a></p>
            <h3>Current Report: <?php echo $report_name; ?></h3>
        </div>
        <?php
        $data = bp_running_log_get_report_data($report, $report_settings);
        //echo "<pre>";
        //var_dump($data);
        //echo "</pre>";
        ?>
        <div class="report">
            <table class="wp-list-table widefat fixed" id="" cellspacing="0">
                <thead>
                    <tr>
                        <th style="width:20px;">#</th>
                        <th>Username</th>
                        <th>Name</th>
                        <th>Numbers</th>
                        <th>Gender</th>
                        <th>Runner Type</th>
                        <th>Region</th>
                        <th>Age Group</th>
                        <th>Distance</th>
                        <th>Pace</th>
                        <th>Lifetime Distance</th>
                        <th>Lifetime Pace</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Name</th>
                        <th>Numbers</th>
                        <th>Gender</th>
                        <th>Runner Type</th>
                        <th>Region</th>
                        <th>Age Group</th>
                        <th>Distance</th>
                        <th>Pace</th>
                        <th>Lifetime Distance</th>
                        <th>Lifetime Pace</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php if (empty($data)) {
                        ?>
                        <tr><td colspan="8">No Information</td></tr>
                        <?php
                    } else {
                        $nr = 0;
                        foreach ($data as $value) {
                            $nr++;
                            $user_data = get_userdata($value['user_id'])->data;
                            $year = date("Y");
                            $birthday = bp_get_profile_field_data('field=Birthday&user_id=' . $value['user_id']);
                            if ($birthday && !empty($birthday)) {
                                $dobtime = strtotime($birthday);
                                $agegroup = date('c', $dobtime);
                                $age = $year - date("Y", $dobtime);
                                $bday = date("m/d/", $dobtime) . $year;
                                if ((time() - strtotime($bday)) < 0)
                                    $age = $age - 1;

                                if ($age < 40) {
                                    $agegroup = 'Under 40';
                                } elseif ($age < 50) {
                                    $agegroup = 'Under 50';
                                } elseif ($age < 60) {
                                    $agegroup = 'Under 60';
                                } elseif ($age < 70) {
                                    $agegroup = 'Under 70';
                                } elseif ($age < 80) {
                                    $agegroup = 'Under 80';
                                } else {
                                    $agegroup = 'Over 80';
                                }
                            } else {
                                $agegroup = '&nbsp;';
                            }
                            $time_interrim = 0;
                            $additional_min = 0;
                            $time_interrim = explode(':', $value['total_distance']['time']);
                            if (count($time_interrim) == 3) {
                                //we have hours, minutes and seconds
                                $additional_min = (int) $time_interrim[0] * 60;
                                if ((int) $additional_min[2] > 29) {
                                    $additional_min + 1; //round the more than half second up
                                }
                                $min = (int) $time_interrim[1] + $additional_min;
                            }
                            $distance_divide = $value['total_distance']['distance'] > 0 ? $value['total_distance']['distance'] : 1;
                            $total_pace = round($min / $distance_divide, 2);
                            ////////////////////
                            $time_interrim2 = 0;
                            $additional_min2 = 0;
                            $time_interrim2 = explode(':', $value['distance']['time']);
                            if (count($time_interrim2) == 3) {
                                //we have hours, minutes and seconds
                                $additional_min2 = (int) $time_interrim2[0] * 60;
                                if ((int) $additional_min2[2] > 29) {
                                    $additional_min2 + 1; //round the more than half second up
                                }
                                $min2 = (int) $time_interrim2[1] + $additional_min2;
                            }
                            $distance_divide2 = $value['distance']['distance'] > 0 ? $value['distance']['distance'] : 1;
                            $pace = round($min2 / $distance_divide2, 2);
                            ?>
                            <tr>
                                <td><?php echo $nr; ?></td>
                                <td><?php echo $user_data->user_login; ?></td>
                                <td><strong><?php echo $user_data->display_name; ?></strong></td>
                                <td><span style="display:inline-block;width:70px;">Permanent:</span> <?php echo bp_get_profile_field_data('field=Permanent Number&user_id=' . $value['user_id']); ?><br />
                                    <span style="display:inline-block;width:70px;">Temp:</span> <?php echo bp_get_profile_field_data('field=Temporary Number&user_id=' . $value['user_id']); ?><br />
                                    <span style="display:inline-block;width:70px;">ASA:</span> <?php echo bp_get_profile_field_data('field=Your ASA licence no.&user_id=' . $value['user_id']); ?></td>
                                <td><?php echo bp_get_profile_field_data('field=Gender&user_id=' . $value['user_id']); ?></td>
                                <td><?php echo bp_get_profile_field_data('field=Running Types&user_id=' . $value['user_id']); ?></td>
                                <td><?php echo bp_get_profile_field_data('field=Province&user_id=' . $value['user_id']); ?></td>
                                <td><?php echo $agegroup; ?></td>
                                <td style="text-align: right"><?php echo $value['distance']['distance']; ?> KM</td>
                                <td style="text-align: right"><?php echo $pace; ?> Min/KM<br />Time: <?php echo $value['distance']['time']; ?></td>
                                <td style="text-align: right"><?php echo $value['total_distance']['distance']; ?> KM</td>
                                <td style="text-align: right"><?php echo $total_pace; ?> Min/KM<br />Time: <?php echo $value['total_distance']['time']; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php
}

function pb_runninglog_comrades_dates() {
    if (isset($_POST['comrades_day'])) {
        //we have a form submission
        $day = (int) $_POST['comrades_day'];
        $month = (int) $_POST['comrades_month'];
        $year = (int) $_POST['comrades_year'];
        bp_runninglog_save_comrades_date($year, $month, $day);
    }
    ?>
    <div class="wrap">
        <h2><?php _e('Comrades Dates', 'bp-runninglog') ?></h2>
        <?php
        $data = bp_runninglog_get_all_comrades_dates();
        $months = array(
            1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December',
        );
        ?>
        <div class="report">
            <div style="float:right; width:50%;">
                <p>To overwrite a Comrades date, select the correct date from the lists at the top (the year is the important part) and save.
                    It will overwrite the existing date with the new one.</p>
                <p>These dates are used on the front-end to determine in which Year which log should be.</p>
                <p>The default Date will be 1 June, and will be used if no other date is specified here.</p>
            </div>
            <form action="" method="post">
                <div class="insert-comrades-date">
                    <label for="comrades_month">Comrades Date:</label><br />
                    <select name="comrades_day" id="comrades_day">
                        <?php
                        for ($i = 1; $i < 32; ++$i) {
                            echo '<option value="' . $i . '"' . selected(1, $i, false) . '>' . $i . '</option>';
                        }
                        ?>
                    </select>
                    <select name="comrades_month" id="comrades_month">
                        <option value="5" selected="selected">May</option>
                        <option value="6">June</option>
                    </select>
                    <select name="comrades_year" id="comrades_year">
                        <?php
                        for ($i = (date('Y') + 1); $i > 1989; $i--) {
                            echo '<option value="' . $i . '">' . $i . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="">
                    <input type="submit" class="button button-primary button-large" value="Save Comrades Date" />
                </div>
            </form>
            <p>&nbsp;</p>
            <table class="wp-list-table widefat fixed" style="width:auto;" id="" cellspacing="0">
                <thead>
                    <tr>
                        <th>Day</th>
                        <th>Month</th>
                        <th>Year</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Day</th>
                        <th>Month</th>
                        <th>Year</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    foreach ($data as $year => $info) {
                        ?>
                        <tr>
                            <td><?php echo $info[1]; ?></td>
                            <td><?php echo $months[$info[0]]; ?></td>
                            <td><?php echo $year; ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php
}

/**
 * Test to see if the necessary database tables are installed, and if not, install them
 *
 * You will only need a function like this if you need to install database tables. It is not
 * recommended that you do so if you can help it; it clutters up users' databases, and it creates
 * problems when attempting to interact with the rest of WordPress. You are highly encouraged
 * to use WordPress custom post types instead.
 *
 * Doing this check in the admin, instead of at activation time, adds a bit of overhead. But the
 * WordPress core developers have expressed a dislike for activation functions, so we do it this
 * way instead. Don't worry - dbDelta() is quite smart about not overwriting anything.
 *
 * @package BuddyPress_Skeleton_Component
 * @since 1.6
 */
function bp_runninglog_install_tables() {
    global $wpdb;

    if (!is_super_admin())
        return;

    if (!empty($wpdb->charset))
        $charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
    $sql = array();
    $sql[] = "CREATE TABLE IF NOT EXISTS {$wpdb->base_prefix}bp_runninglog (
            id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
            user_id bigint(20) NOT NULL,
            date_year text,
            date_month text,
            type enum('event','pre_log'),
            distance bigint(20) NOT NULL,
            date_recorded datetime NOT NULL,
        KEY user_id (user_id)
       ) {$charset_collate};";

    $sql[] = "CREATE TABLE IF NOT EXISTS {$wpdb->base_prefix}bp_award_rels (
            id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
            user_id bigint(20) NOT NULL,
            award_id bigint(20) NOT NULL
        KEY user_id (user_id),
        KEY award_id (award_id)
       ) {$charset_collate};";

    require_once( ABSPATH . 'wp-admin/upgrade.php' );

    dbDelta($sql);

    update_site_option('bp-runninglog-db-version', BP_RUNNINGLOG_DB_VERSION);
}

//add_action( 'admin_init', 'bp_runninglog_install_tables' );
#EOF