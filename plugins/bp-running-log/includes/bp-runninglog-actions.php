<?php
add_action('wp_loaded', 'bp_runninglog_save_report', 99); //make sure all other plugins/components have started
add_action('wp_loaded', 'bp_runninglog_save_full_report', 100); //make sure all other plugins/components have started

function bp_runninglog_save_report() {
    if (!isset($_GET['export_running_log'])) {
        return; //do nothing
    }
    if (!current_user_can('running_reports')) {
        return; //do nothing since we can not even read a report
    }

    $format = $_GET['format'];
    $limit = $_GET['limit'];
	if(!$limit){
		$explimit = count_users();
		$limit = $explimit["total_users"];
	}
    $report = array(
        'gender' => $_GET['gender'],
        'runnertype' => $_GET['runnertype'],
        'region' => $_GET['region'],
        'agegroups' => $_GET['agegroups'],
        'sort' => $_GET['sort'],
        'year' => $_GET['year']
    );
    $report_settings = array(
        'gender' => array(
            'all' => 'All Genders',
            'male' => 'Male',
            'female' => 'Female',
        ),
        'runnertype' => array(
            'all' => 'All Runner Types',
            'runner' => 'Runner',
            'walker' => 'Walker',
            'wheelchair' => 'Wheelchair',
        ),
        'region' => array(
            'all' => 'All Regions',
            '1' => 'Central Gauteng &amp; Vaal Triangle',
            '2' => 'Gauteng North',
            '3' => 'Boland, Border, Eastern Province, South Western Districts, Western Province',
            '4' => 'Mpumulanga, KwaZulu Natal, Limpopo, Central North West, Free State, Griqualand West'
        ),
        'agegroups' => array(
            'all' => 'All Age Groups',
            'under40' => 'Under 40',
            '40-49' => '40 to 49',
            '50-59' => '50 to 59',
            '60-69' => '60 to 69',
            '70-79' => '70 to 79',
            '80plus' => '80+',
        ),
        'time' => array(),
        'sort' => array(
            'desc' => 'Descending Order',
            'asc' => 'Ascending Order'
        )
    );
    $data = bp_running_log_get_report_data($report, $report_settings);

    switch ($format) {
        case 'html':
            echo "<h3>" . bp_runninglog_get_report_name($report, $report_settings) . '</h3>';
            ?>
            <table class="wp-list-table widefat fixed" id="" cellspacing="0">
                <thead>
                    <tr>
                        <th style="width:20px;">#</th>
                        <th>Username</th>
                        <th>Name</th>
                        <th>Numbers</th>
                        <th>Gender</th>
                        <th>Runner Type</th>
                        <th>Region</th>
                        <th>Age Group</th>
                        <th>Distance</th>
                        <th>Pace</th>
                        <th>Lifetime Distance</th>
                        <th>Lifetime Pace</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th style="width:20px;">#</th>
                        <th>Username</th>
                        <th>Name</th>
                        <th>Numbers</th>
                        <th>Gender</th>
                        <th>Runner Type</th>
                        <th>Region</th>
                        <th>Age Group</th>
                        <th>Distance</th>
                        <th>Pace</th>
                        <th>Lifetime Distance</th>
                        <th>Lifetime Pace</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php if (empty($data)) {
                        ?>
                        <tr><td colspan="8">No Information</td></tr>
                        <?php
                    } else {
                        $nr = 0;
                        foreach ($data as $value) {
                            $nr++;
                            $user_data = get_userdata($value['user_id'])->data;
                            $year = date("Y");
                            $birthday = bp_get_profile_field_data('field=Birthday&user_id=' . $value['user_id']);
                            if ($birthday && !empty($birthday)) {
                                $dobtime = strtotime($birthday);
                                $agegroup = date('c', $dobtime);
                                $age = $year - date("Y", $dobtime);
                                $bday = date("m/d/", $dobtime) . $year;
                                if ((time() - strtotime($bday)) < 0)
                                    $age = $age - 1;

                                if ($age < 40) {
                                    $agegroup = 'Under 40';
                                } elseif ($age < 50) {
                                    $agegroup = 'Under 50';
                                } elseif ($age < 60) {
                                    $agegroup = 'Under 60';
                                } elseif ($age < 70) {
                                    $agegroup = 'Under 70';
                                } elseif ($age < 80) {
                                    $agegroup = 'Under 80';
                                } else {
                                    $agegroup = 'Over 80';
                                }
                            } else {
                                $agegroup = '&nbsp;';
                            }
                            $time_interrim = 0;
                            $additional_min = 0;
                            $time_interrim = explode(':', $value['total_distance']['time']);
                            if (count($time_interrim) == 3) {
                                //we have hours, minutes and seconds
                                $additional_min = (int) $time_interrim[0] * 60;
                                if ((int) $additional_min[2] > 29) {
                                    $additional_min + 1; //round the more than half second up
                                }
                                $min = (int) $time_interrim[1] + $additional_min;
                            }
                            $distance_divide = $value['total_distance']['distance'] > 0 ? $value['total_distance']['distance'] : 1;
                            $total_pace = round($min / $distance_divide, 2);
                            ////////////////////
                            $time_interrim2 = 0;
                            $additional_min2 = 0;
                            $time_interrim2 = explode(':', $value['distance']['time']);
                            if (count($time_interrim2) == 3) {
                                //we have hours, minutes and seconds
                                $additional_min2 = (int) $time_interrim2[0] * 60;
                                if ((int) $additional_min2[2] > 29) {
                                    $additional_min2 + 1; //round the more than half second up
                                }
                                $min2 = (int) $time_interrim2[1] + $additional_min2;
                            }
                            $distance_divide2 = $value['distance']['distance'] > 0 ? $value['distance']['distance'] : 1;
                            $pace = round($min2 / $distance_divide2, 2);
                            ?>
                            <tr>
                                <td><?php echo $nr; ?></td>
                                <td><?php echo $user_data->user_login; ?></td>
                                <td><strong><?php echo $user_data->display_name; ?></strong></td>
                                <td><span style="display:inline-block;width:70px;">Permanent:</span> <?php echo bp_get_profile_field_data('field=Permanent Number&user_id=' . $value['user_id']); ?><br />
                                    <?php //<span style="display:inline-block;width:70px;">Temp:</span> <?php echo bp_get_profile_field_data('field=Temporary Number&user_id=' . $value['user_id']); ? ><br /> ?>
                                    <span style="display:inline-block;width:70px;">ASA:</span> <?php echo bp_get_profile_field_data('field=Your ASA licence no.&user_id=' . $value['user_id']); ?></td>
                                <td><?php echo bp_get_profile_field_data('field=Gender&user_id=' . $value['user_id']); ?></td>
                                <td><?php echo bp_get_profile_field_data('field=Running Types&user_id=' . $value['user_id']); ?></td>
                                <td><?php echo bp_get_profile_field_data('field=Province&user_id=' . $value['user_id']); ?></td>
                                <td><?php echo $agegroup; ?></td>
                                <td style="text-align: right"><?php echo $value['distance']['distance']; ?> KM</td>
                                <td style="text-align: right"><?php echo $pace; ?> Min/KM<br />Time: <?php echo $value['distance']['time']; ?></td>
                                <td style="text-align: right"><?php echo $value['total_distance']['distance']; ?> KM</td>
                                <td style="text-align: right"><?php echo $total_pace; ?> Min/KM<br />Time: <?php echo $value['total_distance']['time']; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
            <?php
            exit();
            break;
        case 'csv':
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header('Content-Description: File Transfer');
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=runninglog_report.csv");
            header("Expires: 0");
            header("Pragma: public");

            $array = array();
            $array[0][] = bp_runninglog_get_report_name($report, $report_settings);
            ;
            $array[1][] = '#';
            $array[1][] = 'Username';
            $array[1][] = 'Name';
            $array[1][] = 'Numbers';
            $array[1][] = 'Gender';
            $array[1][] = 'Runner Type';
            $array[1][] = 'Region';
            $array[1][] = 'Age Group';
            $array[1][] = 'Distance';
            $array[1][] = 'Pace';
            $array[1][] = 'Lifetime Distance';
            $array[1][] = 'Lifetime Pace';

            if (empty($data)) {
                $array[] = array('No Information');
            } else {
                $nr = 0;
                foreach ($data as $value) {
                    $nr++;
                    if ($nr > $limit) {
                        break;
                    }
                    $user_data = get_userdata($value['user_id'])->data;
                    $year = date("Y");
                    $birthday = bp_get_profile_field_data('field=Birthday&user_id=' . $value['user_id']);
                    if ($birthday && !empty($birthday)) {
                        $dobtime = strtotime($birthday);
                        $agegroup = date('c', $dobtime);
                        $age = $year - date("Y", $dobtime);
                        $bday = date("m/d/", $dobtime) . $year;
                        if ((time() - strtotime($bday)) < 0)
                            $age = $age - 1;

                        if ($age < 40) {
                            $agegroup = 'Under 40';
                        } elseif ($age < 50) {
                            $agegroup = 'Under 50';
                        } elseif ($age < 60) {
                            $agegroup = 'Under 60';
                        } elseif ($age < 70) {
                            $agegroup = 'Under 70';
                        } elseif ($age < 80) {
                            $agegroup = 'Under 80';
                        } else {
                            $agegroup = 'Over 80';
                        }
                    } else {
                        $agegroup = ' ';
                    }
                    $time_interrim = 0;
                    $additional_min = 0;
                    $time_interrim = explode(':', $value['total_distance']['time']);
                    if (count($time_interrim) == 3) {
                        //we have hours, minutes and seconds
                        $additional_min = (int) $time_interrim[0] * 60;
                        if ((int) $additional_min[2] > 29) {
                            $additional_min + 1; //round the more than half second up
                        }
                        $min = (int) $time_interrim[1] + $additional_min;
                    }
                    $distance_divide = $value['total_distance']['distance'] > 0 ? $value['total_distance']['distance'] : 1;
                    $total_pace = round($min / $distance_divide, 2);
                    ////////////////////
                    $time_interrim2 = 0;
                    $additional_min2 = 0;
                    $time_interrim2 = explode(':', $value['distance']['time']);
                    if (count($time_interrim2) == 3) {
                        //we have hours, minutes and seconds
                        $additional_min2 = (int) $time_interrim2[0] * 60;
                        if ((int) $additional_min2[2] > 29) {
                            $additional_min2 + 1; //round the more than half second up
                        }
                        $min2 = (int) $time_interrim2[1] + $additional_min2;
                    }
                    $distance_divide2 = $value['distance']['distance'] > 0 ? $value['distance']['distance'] : 1;
                    $pace = round($min2 / $distance_divide2, 2);
                    $array[] = array(
                        $nr,
                        $user_data->user_login,
                        $user_data->display_name,
                        bp_get_profile_field_data('field=Permanent Number&user_id=' . $value['user_id']) . ' ' .
                        //bp_get_profile_field_data('field=Temporary Number&user_id=' . $value['user_id']) . ' ' .
                        bp_get_profile_field_data('field=Your ASA licence no.&user_id=' . $value['user_id']),
                        bp_get_profile_field_data('field=Gender&user_id=' . $value['user_id']),
                        bp_get_profile_field_data('field=Running Types&user_id=' . $value['user_id']),
                        bp_get_profile_field_data('field=Province&user_id=' . $value['user_id']),
                        $agegroup,
                        $value['distance']['distance'] . ' KM',
                        $pace . ' Min/KM' . '; Time: ' . $value['distance']['time'],
                        $value['total_distance']['distance'] . ' KM',
                        $total_pace . ' Min/KM' . '; Time: ' . $value['total_distance']['time'],
                    );
                }
            }
            bp_running_log_generateCsv($array);
            break;
    }
}
function bp_runninglog_save_full_report() {
    if (!isset($_GET['export_full_running_log'])) {
        return; //do nothing
    }
    if (!current_user_can('running_reports')) {
        return; //do nothing since we can not even read a report
    }
    $report = array(
        'year' => $_GET['full_log_year']
    );
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header('Content-Description: File Transfer');
    header("Content-type: text/csv");
    header("Content-Disposition: attachment; filename=runninglog_full_report-".date('Y_m_d').".csv");
    header("Expires: 0");
    header("Pragma: public");

    $array = array();
    $array[0][] = 'Full Report for '.$report['year'];
    $array[1][] = 'Username';
    $array[1][] = 'Name';
    $array[1][] = 'Date';
    $array[1][] = 'Event';
    $array[1][] = 'Distance KM';
    $array[1][] = 'Time';

    $data = generate_full_report_for_year($report['year']);
    if (empty($data)) {
        $array[] = array('No Information');
    } else {
        foreach ($data as $row) {
            $user_data = get_userdata($row['user_id'])->data;
            $array[] = array(
                $user_data->user_login,
                $user_data->display_name,
                $row['date_year'].'-'.$row['date_month'].'-'.$row['date_day'],
                stripslashes($row['event']),
                $row['distance'],
                $row['time']
            );
        }
    }
    bp_running_log_generateCsv($array);
}

function bp_running_log_generateCsv($data, $delimiter = ',', $enclosure = '"') {
    $handle = fopen('php://output', 'w');
    foreach ($data as $line) {
        fputcsv($handle, $line, $delimiter, $enclosure);
    }
    fclose($handle);
    exit();
}

#EOF