<?php

/**
 * NOTE: You should always use the wp_enqueue_script() and wp_enqueue_style() functions to include
 * javascript and css files.
 */

/**
 * bp_example_add_js()
 *
 * This function will enqueue the components javascript file, so that you can make
 * use of any javascript you bundle with your component within your interface screens.
 */
function bp_runninglog_add_js() {
    //global $bp;
    //if ($bp->current_component == $bp->runninglog->slug)
    //wp_enqueue_script('bp-runninglog-js', plugins_url('/bp-runninglog/js/general.js'));
    wp_enqueue_script('bp-runninglog-js', plugins_url('js/runninglog.js', __FILE__), array('jquery'), null, true);
}

add_action('wp_enqueue_scripts', 'bp_runninglog_add_js', 1);

#EOF