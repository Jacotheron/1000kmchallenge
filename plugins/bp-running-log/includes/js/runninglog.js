jQuery(document).ready(function() {
    jQuery('#runninglog_logs .delete_runninglog').on('click', function(event) {
        var target = jQuery(event.target);
        /* Delete activity stream items */
        var tr = target.parents('#runninglog_logs tr');
        var id = tr.attr('id').substr(7, tr.attr('id').length);
        var link_href = target.attr('href');
        var nonce = link_href.split('_wpnonce=');
        nonce = nonce[1];
        target.addClass('loading');
        jQuery.post(ajaxurl, {
            action: 'delete_runninglog',
            'cookie': encodeURIComponent(document.cookie),
            'id': id,
            '_wpnonce': nonce
        },
        function(response) {

            if (response) {
                jQuery('#ajax_message').html(response).hide().fadeIn(500);
            } else {
                tr.slideUp(500);
            }
        });
        return false;
    });
});