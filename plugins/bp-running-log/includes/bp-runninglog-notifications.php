<?php
/* * ******************************************************************************
 * Activity & Notification Functions
 *
 * These functions handle the recording, deleting and formatting of activity and
 * notifications for the user and for this specific component.
 */

function bp_runninglog_screen_notification_settings() {
    global $current_user;
    ?>
    <table class="notification-settings" id="bp-runninglog-notification-settings">

        <thead>
            <tr>
                <th class="icon"></th>
                <th class="title"><?php _e('Running Log', 'bp-runninglog') ?></th>
                <th class="yes"><?php _e('Yes', 'bp-runninglog') ?></th>
                <th class="no"><?php _e('No', 'bp-runninglog') ?></th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td></td>
                <td><?php _e('A member awards you with an Award', 'bp-runninglog') ?></td>
                <td class="yes"><input type="radio" name="notifications[notification_runninglog_award_awarded]" value="yes" <?php if (!get_user_meta($current_user->ID, 'notification_runninglog_award_awarded', true) || 'yes' == get_user_meta($current_user->ID, 'notification_runninglog_award_awarded', true)) { ?>checked="checked" <?php } ?>/></td>
                <td class="no"><input type="radio" name="notifications[notification_runninglog_award_awarded]" value="no" <?php if (get_user_meta($current_user->ID, 'notification_runninglog_award_awarded') == 'no') { ?>checked="checked" <?php } ?>/></td>
            </tr>

            <?php do_action('bp_runninglog_notification_settings'); ?>

        </tbody>
    </table>
    <?php
}

add_action('bp_notification_settings', 'bp_runninglog_screen_notification_settings');

function bp_runninglog_remove_screen_notifications() {
    global $bp;

    /**
     * When clicking on a screen notification, we need to remove it from the menu.
     * The following command will do so.
     */
    bp_core_delete_notifications_for_user_by_type($bp->loggedin_user->id, $bp->runninglog->slug, 'new_award');
}

/**
 * Notification functions are used to send email notifications to users on specific events
 * They will check to see the users notification settings first, if the user has the notifications
 * turned on, they will be sent a formatted email notification.
 *
 * You should use your own custom actions to determine when an email notification should be sent.
 */
function bp_runninglog_send_award_awarded_notification($to_user_id, $from_user_id, $award_name, $reason = '') {
    global $bp;

    /* Let's grab both user's names to use in the email. */
    $sender_name = bp_core_get_user_displayname($from_user_id, false);
    $reciever_name = bp_core_get_user_displayname($to_user_id, false);

    /* We need to check to see if the recipient has opted not to recieve high-five emails */
    if ('no' == get_user_meta((int) $to_user_id, 'notification_runninglog_award_awarded', true))
        return false;

    /* Get the userdata for the reciever and sender, this will include usernames and emails that we need. */
    $reciever_ud = get_userdata($to_user_id);
    $sender_ud = get_userdata($from_user_id);

    /* Now we need to construct the URL's that we are going to use in the email */
    $sender_profile_link = site_url(BP_MEMBERS_SLUG . '/' . $sender_ud->user_login . '/' . $bp->profile->slug);
    $sender_highfive_link = site_url(BP_MEMBERS_SLUG . '/' . $sender_ud->user_login . '/' . $bp->example->slug . '/screen-one');
    $reciever_settings_link = site_url(BP_MEMBERS_SLUG . '/' . $reciever_ud->user_login . '/settings/notifications');

    /* Set up and send the message */
    $to = $reciever_ud->user_email;
    $subject = '[' . get_blog_option(1, 'blogname') . '] ' . sprintf(__('%s Awarded you the %s Award', 'bp-runninglog'), stripslashes($sender_name), $award_name);

    $message = sprintf(__(
                    '%1s Gave you the %3s Award

%2s

---------------------
', 'bp-runninglog'), $sender_name, $reason, $award_name);

    $message .= sprintf(__('To disable these notifications please log in and go to: %s', 'bp-runninglog'), $reciever_settings_link);

    // Send it!
    wp_mail($to, $subject, $message);
}

add_action('bp_runninglog_award_award', 'bp_runninglog_award_awarded_notification', 10, 2);

#EOF