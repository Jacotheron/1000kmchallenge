<?php
/**
 * Plugin Name: 1000kmC
 * Description: 
 * Version: 1.0.0
 * Author: Jacotheron
 * Plugin Type: Piklist
 */
add_action( 'init', 'themeplugin_init' );
function themeplugin_init() {
		if ( is_admin() ) {
				include_once( plugin_dir_path( __FILE__ ) . 'class-piklist-checker.php' );
				if ( ! piklist_checker::check( __FILE__ ) ) {
						return;
				}
		}
		add_shortcode('page_attachments','themeplugin_page_attachments');
}
function themeplugin_page_attachments($atts, $content = '', $shortcode = ''){
	$this_page = get_the_ID();
	if(!$this_page){
		return $content;
	}
	$page_slug = get_post_field('post_name',$this_page,'attribute');
	$files = get_post_meta($this_page, 'attachments', false);
	
	$column_class = 'col-xs-6 col-sm-4 col-md-3 col-lg-2 attachment-block';
	$output .= '<div class="container-fluid">';
		$output .= '<div class="row">';
			$loops = 0;
			foreach($files as $file_id){
				if($file_id == 'undefined'){
					continue;
				}
				$loops++;
				$image = wp_get_attachment_image($file_id,'thumbnail',true);
				$link = wp_get_attachment_url($file_id);
				$name = get_post_field('post_title',$file_id,'display');
				$output .= '<div class="'.$column_class.'">';
					$output .= '<div class="page_atachment">';
						$output .= '<div class="filename">';
							$output .= '<a href="'.$link.'" target="_blank">'.$name.'</a>';
						$output .= '</div>';
						$output .= '<a href="'.$link.'" target="_blank">'.$image.'</a>';
					$output .= '</div>';
				$output .= '</div>';
			}
			if($loops == 0){
				$output .= '<div class="col-sm-12"><p>No files found for this section.</p></div>';
			}
		$output .= '</div>';
	$output .= '</div>';
	$output .= ''.$content;
	return $output;
}