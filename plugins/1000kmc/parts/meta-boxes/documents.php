<?php
/*
Title: Document Attachments
Post Type: page
*/

piklist('field', array(
    'type' => 'file',
    'field' => 'attachments',
    'label' => 'Add File(s)',
    'description' => 'Upload and select attachments for this section',
	'scope' => 'post_meta',
	'options' => array(
		'modal_title' => __( 'Attachments', 'piklist' ),
		'button'      => __( 'Add Attachments', 'piklist' )
	)
));